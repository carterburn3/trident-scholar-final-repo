from ple import PLE
import frogger
import numpy as np
from pygame.constants import K_w,K_a,K_F15

class NaiveAgent():
    def __init__(self, actions):
        self.actions = actions
        self.step = 0

    def pickAction(self, reward, obs):
        #return K_a
        return K_F15
        return self.actions[np.random.randint(0,len(self.actions))]

game = frogger.Frogger()
fps = 30
p = PLE(game, fps=fps,force_fps=False)
agent = NaiveAgent(p.getActionSet())
reward = 0.0

#p.init()

while True:
    if p.game_over():
        p.reset_game()

    obs = p.getGameState()
    print obs['frog_x'],obs['frog_y']
    action = agent.pickAction(reward, obs)
    reward = p.act(action)
    #print game.score
