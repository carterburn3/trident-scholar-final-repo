from ple import PLE
import frogger
from frog_sprites import Turtle
import numpy as np
from pygame.constants import K_w,K_a,K_F15
from pygame import Rect,sprite
from constants import *
from copy import deepcopy

class NonLinearFeatures:
    def __init__(self):
        self.numCols = 3
        self.numRows=3

    def getFN(self, i, j):
         num=3*j+i
         if num >= 4: num-=1
         return num

    def getFeatures(self, state):
        features=dict()
        fRect=Rect((state['frog_x'],state['frog_y']),(32.0,32.0))
        sX=float(state['frog_x']-32.0)
        sY=float(state['frog_y']-32.0) # start x and y for the 32x32 pixel boxes
        # set frog_y value as 4th feature
        print state['frog_y']
        features[16]=float(int(state['frog_y']/32)/15.0) # not sure if this should be /32 or not (definitely want int) # normalized to be in range [0,1]
        # finding sprites in our surrounding boxes
        boxes=[None]*self.numCols
        for i in range(self.numCols):
            boxes[i]=[None]*self.numRows

        # create 32x32 pixel boxes around frog
        for i in range(self.numCols):
            for j in range(self.numRows):
                if (i != 1 or j != 1):
                    boxes[i][j]=Rect((sX+(32.0*i),sY+(32.0*j)),(32.0,32.0))

        # check collisions within each box
        for i in range(self.numCols):
            for j in range(self.numRows):
                if boxes[i][j]==None: continue
                if boxes[i][j].collidelist(state['cars']) != -1:
                    # this box has a collision with a car, set feature value to 1
                    features[self.getFN(i,j)]=float(1)
                    features[self.getFN(i,j)+8] = float(0)
                elif boxes[i][j].collidelist(state['rivers']) != -1:
                    '''# this box has a collision with a river obj, set feature value to 1
                    if self.getFN(i,j)+8 == 11 or self.getFN(i,j)+8 == 12:
                        # this box fired, check if boxes[2][2] collides with the same object
                        sideColl=state['rivers'][boxes[i][j].collidelist(state['rivers'])] # get the rect that collided
                        coll=state['rivers'][fRect.collidelist(state['rivers'])] # get the rect the frog is currently on
                        if sideColl == coll:
                            # they're the same box, this is okay, set as we would any feature
                            features[self.getFN(i,j)+8]=float(1)
                            features[self.getFN(i,j)] = float(0)
                        else:
                            # they're different boxes, set this feature value to 0
                            features[self.getFN(i,j)+8]=float(0)
                            features[self.getFN(i,j)] = float(0)
                else:'''
                    features[self.getFN(i,j)+8]=float(1)
                    features[self.getFN(i,j)] = float(0)

                    '''# check for diving turtle
                    #coll=state['rivers'][boxes[i][j].collidelist(state['rivers'])]
                    coll=sprite.spritecollide(state['frog'],state['rivergrp'],False)
                    if len(coll) != 0 and isinstance(coll[0], Turtle) and (coll[0].animateIndex == 3 or coll[0].animateIndex==4):
                        print 'diving!'
                        features[self.getFN(i,j)+8]=float(0)
                        features[self.getFN(i,j)]=float(0)'''
                else:
                    # this box does not have a collision, set feature value to 0
                    features[self.getFN(i,j)]=int(0)
                    features[self.getFN(i,j)+8]=int(0)

        if state['frog_y'] <= kPlayYRiver[4]:
            # need to check the status of homes, if we collide with an
            # open home square in one of the top boxes, then we are
            # clear (0, 0) in the 0,1,2 features; otherwise its a 1
            # homeR only gives open rects, thus if we collide with that
            # we are clear
            if boxes[0][0].collidelist(state['homeR']) != -1:
                # collided in feature 0, clear
                features[8] = float(1)
            else: features[8] = float(0)
            if boxes[1][0].collidelist(state['homeR']) != -1:
                features[9] = float(1)
            else: features[9] = float(0)
            if boxes[2][0].collidelist(state['homeR']) != -1:
                features[10] = float(1)
            else: features[10] = float(0)

        if state['frog_y'] <= kPlayYRiverLimit and state['frog_x'] <= 0.0:
            # left side is about to hit, raise collision in the left
            # features (0,3,5)
            features[0]=float(1)
            features[3]=float(1)
            features[5]=float(1)
        if state['frog_y'] <= kPlayYRiverLimit and state['frog_x'] >= 430.0:
            # right side is about to hit
            features[2]=float(1)
            features[4]=float(1)
            features[7]=float(1)

        '''# now we check for river objects by moving a frog sprite around
        # motivate frog
        newFrog=deepcopy(state['frog'])
        # frog is at start location, now we need to move it around and see if it collides
        # if it currently is attached, then we need to only allow it to move laterally
        # on the attached object
        for i in range(self.numCols):
            for j in range(self.numRows):
                if (i != 1 or j != 1):
                    # set position of frog
                    newFrog.set_pos((sX+(32.0*i),sY+(32.0*j)))
                    h=sprite.spritecollide(newFrog,state['riverS'],False)
                    if len(h) == 0:
                        # no collision in this feature value
                        features[self.getFN(i,j)+8] = float(0)
                        features[self.getFN(i,j)] = float(0)
                    elif (self.getFN(i,j)+8 == 11) or (self.getFN(i,j)+8 == 12):
                        # collision, but sideways, must be colliding with attached
                        print h[0],newFrog.attachedObj
                        if h[0].get_pos() == newFrog.attachedObj.get_pos():
                            features[self.getFN(i,j)+8] = float(1)
                            features[self.getFN(i,j)] = float(0)
                        else:
                            features[self.getFN(i,j)+8] = float(0)
                            features[self.getFN(i,j)] = float(0)
                    else:
                        # collision in another box, set feature
                        features[self.getFN(i,j)+8] = float(1)
                        features[self.getFN(i,j)] = float(0)'''

        # add the home status
        #for i in range(9,14):
        #    features[i]=int(state['homes'][i-9])

        #features['bias']=int(1)

        return features

class NaiveAgent():
    def __init__(self, actions):
        self.actions = actions
        self.step = 0
        self.noop = K_F15

    def pickAction(self, reward, obs):
        #return K_a
        return self.noop
        return self.actions[np.random.randint(0,len(self.actions))]

def featsMatVec(feats):
    featsMat=np.zeros((16,16))
    featsVec=np.zeros((16,1))
    for key in feats.keys():
        if key >= 0 and key < 16:
            # add to our matrix
            featsMat[int(key)][16-(16-int(key)):] = feats[key]
        if key >= 1 and key < 17:
            featsVec[int(key)-1][0] = feats[key]

    # squares of each weight
    sqrVec=np.zeros((1,17))
    for key in feats.keys():
        if key >= 0 and key < 17:
            sqrVec[0][int(key)]=feats[key]*feats[key]

    return featsMat,featsVec,sqrVec

game = frogger.Frogger()
fps = 30
p = PLE(game, fps=fps,force_fps=False)
agent = NaiveAgent(p.getActionSet())
reward = 0.0
features=NonLinearFeatures()

#p.init()

while True:
    if p.game_over():
        p.reset_game()

    obs = p.getGameState()
    feats = features.getFeatures(obs)
    print feats
    x,y,z = featsMatVec(feats)
    #print x,'\n',y,'\n',z
    action = agent.pickAction(reward, obs)
    reward = p.act(action)
    #print reward
    #print game.score
