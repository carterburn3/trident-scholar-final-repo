import pygame
from pygame.constants import K_F15,K_SPACE,K_UP,K_DOWN,K_LEFT,K_RIGHT,KEYDOWN,K_RETURN,K_KP_ENTER,K_q
from constants import *
from time import sleep
from frog_sprites import Frog
from copy import deepcopy
import sys

class HumanActions():
    def __init__(self, bgd, n):
        self.bgd=bgd
        self.nextAction = None
        self.n=n
        self.teacherCount=0
        self.lateral=False
        self.messages=['Agent Training']

    def setGame(self, game):
        self.game = game

    def showExpOver(self):
        self.game.screen.fill((0,0,0))
        message='Experiment is over! Moving on to Curriculum Development.'
        x=(kScreenWidth-self.game.backdrop.font.size(message)[0])/2
        label=self.game.backdrop.font.render(message, 1, (36,253,46))
        self.game.screen.blit(label, (x, kScreenHeight/2))
        pygame.display.update()
        sleep(5)

    def timeWarp(self, savedStates):
        # need to first find where we are in terms of the deaths, we want to move
        # 50 states back, if we find a death, then we move another 50
        # DECREMENT FROM THE LAST INDEX FIRST, NOT SHOWING UPDATES
        currState=len(savedStates)-1
        limit=currState-50

        #self.messages=['Reversing Time']

        self.game.show=False
        while currState >= limit and currState >= 0:
            # decrement, without showing
            self.game.timeWarpStep(savedStates[currState])
            if self.game.lives <= 0:
                # had a death, we can keep going back
                self.game.init()
                self.game.humanWaiting=True
                self.game.human=True
                self.game.show=False
                limit = limit - 50
            currState-=1

        # this is where we are at, move back to the current state
        self.game.show=False
        for i in range(currState, len(savedStates)):
            self.game.timeWarpStep(savedStates[i])
            if self.game.lives <= 0:
                self.game.init()
                self.game.humanWaiting=True
                self.game.human=True
                self.game.show=False

        # back to the current state, allow user to move backwards
        self.game.show=True
        self.game.drawUpdates()

        stateInd=len(savedStates)-1
        if limit < 0: limit = 0

        self.messages=['Press Space to Start Training', ' ',
                       'Use Arrow Keys or the Mouse Scroller to select a state',
                       ' ']
        self.setSAMsg(str(stateInd-limit))
        self.game.backdrop.leftOffset.fill((0,0,0))
        self.game.backdrop.draw_messageCenter(self.messages)
        pygame.display.update()
        
        while True:
            e=pygame.event.wait()
            if e.type == KEYDOWN:
                if e.key == K_q:
                    # quitting need to signal to the quadratic we're done
                    return None
                if e.key == K_UP:
                    # increment here
                    stateInd = self.increment(stateInd, savedStates, limit)
                if e.key == K_DOWN:
                    # decrement here
                    stateInd = self.decrement(stateInd, savedStates, limit)
                #if e.key == K_RETURN or e.key == K_KP_ENTER:
                if e.key == K_SPACE:
                    # consider letting user pick the first action
                    self.messages=['Human Training', ' ', 'Use the Arrow Keys to Take Action!']
                    return (savedStates[stateInd][28],savedStates[stateInd][29],
                            savedStates[stateInd][31],savedStates[stateInd][32]) # location of the weights at this index
            if e.type == pygame.MOUSEBUTTONUP:
                if e.button == 4:
                    # increment here
                    stateInd = self.increment(stateInd, savedStates, limit)
                if e.button == 5:
                    # decrement here
                    stateInd = self.decrement(stateInd, savedStates, limit)

    def increment(self, stateInd, savedStates, limit):
        if stateInd == len(savedStates)-1:
            if len(self.messages) < 5:
                self.messages.append('Can\'t move up any further!')
            else:
                self.messages[4] = 'Can\'t move up any further!'
            # update the screen
            self.game.backdrop.leftOffset.fill((0,0,0))
            self.game.backdrop.draw_messageCenter(self.messages)
            pygame.display.update()
            sleep(0.5)
            self.setSAMsg(str(stateInd-limit))
            self.game.backdrop.leftOffset.fill((0,0,0))
            self.game.backdrop.draw_messageCenter(self.messages)
            pygame.display.update()
            return stateInd
        # increment the state index and draw that next state
        stateInd+=1
        self.setSAMsg(str(stateInd-limit))
        self.game.timeWarpStep(savedStates[stateInd])
        # check if this is a death state
        if self.game.lives <= 0:
            # death state, wait a second, then increment again
            # need to check so index doesn't go out of bounds, it shouldn't
            # as there's no way the user would be able to get here, but
            # need to be careful
            if stateInd == len(savedStates)-1:
                print 'You found an impossible warp state, moving back'
                stateInd-=1
                self.setSAMsg(str(stateInd-limit))
                self.game.timeWarpStep(savedStates[stateInd])
                return stateInd
            sleep(0.5)
            self.game.init()
            self.game.human=True
            self.game.humanWaiting=True
            stateInd+=1
            self.setSAMsg(str(stateInd-limit))
            self.game.timeWarpStep(savedStates[stateInd])
            return stateInd
        else:
            # not a death state, just return
            return stateInd

    def decrement(self, stateInd, savedStates, limit):
        if stateInd == 0 or stateInd == limit:
            return stateInd
        stateInd-=1
        self.setSAMsg(str(stateInd-limit))
        self.game.timeWarpStep(savedStates[stateInd])
        # check if death state
        if self.game.lives <= 0:
            # wait a second, then decrement again
            # first check to make sure we can decrement again
            if stateInd == 0:
                print 'Can\'t decrement any more states, moving back to after death'
                sleep(0.5)
                self.game.init()
                self.game.human=True
                self.game.humanWaiting=True
                stateInd+=1
                self.setSAMsg(str(stateInd-limit))
                self.game.timeWarpStep(savedStates[stateInd])
                return stateInd
            sleep(0.5)
            self.game.init()
            self.game.human=True
            self.game.humanWaiting=True
            stateInd-=1
            self.setSAMsg(str(stateInd-limit))
            self.game.timeWarpStep(savedStates[stateInd])
            return stateInd
        else:
            # not a death state, just return
            return stateInd

    def setSAMsg(self, numStr):
        if len(self.messages) < 5:
            self.messages.append('States Available ' + numStr)
        else:
            self.messages[4] = 'States Available ' + numStr
            

    def getActionFromHuman(self):
        if self.n <= 0:
            self.game.human=False
            self.nextAction = None
        if self.nextAction != None:
            act=self.nextAction
            self.nextAction = None
            if not self.game.clock: self.game.clock = pygame.time.Clock()
            if self.game.humanWaiting: self.game.humanWaiting = False
            self.teacherCount+=1
            self.n-=1
            return act
        else:
            self.teacherCount+=1
            self.n-=1
            return K_F15
