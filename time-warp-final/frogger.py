import pygame
from pygame.constants import K_EXCLAIM,K_AMPERSAND,K_DOLLAR,K_HASH,K_F15,K_SPACE,K_UP,K_DOWN,K_LEFT,K_RIGHT
import ple
from ple.games import base
from constants import *
import os, sys, time
from sys import stderr as err
from frog_sprites import *
from supporter import *

class Backdrop:
    def __init__(self, SCREEN_WIDTH, SCREEN_HEIGHT, image_background):
        self.SCREEN_WIDTH = SCREEN_WIDTH
        self.SCREEN_HEIGHT = SCREEN_HEIGHT

        self.background_image = image_background
        self.x = 0
        self.y = 0
        self.leftOffset = None
        self.rightOffset = None
        self.rect = self.background_image.get_rect()
        self.rect.left = self.x + kXOffset
        self.rect.top = self.y

        self.font=pygame.font.SysFont("monospace", 20)
        self.adviceLabel=self.font.render("Advice Budget", 1, (36,253,46))

    def draw_background(self, screen):
        screen.fill((0,0,0))
        screen.blit(self.background_image, (self.rect.left,self.rect.top))

    def draw_outerEdge(self, screen):
        if self.leftOffset == None and self.rightOffset == None:
            self.leftOffset = screen.subsurface(pygame.Rect(0,0,kXOffset,self.SCREEN_HEIGHT))
            self.rightOffset = screen.subsurface(pygame.Rect(kXOffset+kPlayWidth,0,self.SCREEN_WIDTH-(kXOffset+kPlayWidth),self.SCREEN_HEIGHT))
        self.leftOffset.fill((0,0,0))
        self.rightOffset.fill((0,0,0))

    def draw_budgetMessage(self, budget):
        numLabel=self.font.render(str(budget), 1, (36,253,46))
        self.rightOffset.blit(self.adviceLabel, (0, ((kScreenHeight/2)-21)))
        self.rightOffset.blit(numLabel,
                              ((self.adviceLabel.get_rect().width/2)-(numLabel.get_rect().width/2),
                               ((kScreenHeight/2)+21)))

    def splitMessage(self, message):
        # function to get a message into multiple lines
        if len(message) == 0: return []
        extra=[]
        while self.font.size(' '.join(message))[0] > kXOffset-10:
            extra.insert(0,message[-1])
            message=message[:-1]
        return message, extra

    def splitHelper(self, message):
        messages=[]
        ret=self.splitMessage(message)
        while ret[1] != []:
            messages.append(ret[0])
            ret=self.splitMessage(ret[1])
        messages.append(ret[0])
        labs=[]
        for l in messages: labs.append(' '.join(l))
        return labs

    def draw_messageCenter(self, messages):
        # first generate all of the labels that we need for this message center
        labels=[]
        for msg in messages:
            if self.font.size(msg)[0] > kXOffset-10:
                for l in self.splitHelper(msg.split(' ')):
                    labels.append(self.font.render(l, 1, (36,253,46)))
            else:
                labels.append( self.font.render(msg, 1, (36,253,46)) )

        # labels all created, now blit them in the correct place
        y=(kScreenHeight-(self.font.get_linesize()*len(labels)))/2
        for lab in labels:
            self.leftOffset.blit(lab, ( (self.leftOffset.get_rect().width/2)-(lab.get_rect().width/2),
                                        y))
            y+=self.font.get_linesize()

class Frogger(base.PyGameWrapper):
    def __init__(self, width=kScreenWidth, height=kScreenHeight):
        actions = {
            "up": K_EXCLAIM,
            "right": K_HASH,
            "down": K_DOLLAR,
            "left": K_AMPERSAND
        }

        fps = 30
        base.PyGameWrapper.__init__(self, width, height, actions=actions)

        self.images={}
        pygame.display.set_mode((1,1),pygame.NOFRAME)

        self.support = Supporter(self)
        
        self._dir_ = os.path.dirname(os.path.abspath(__file__))
        self._data_dir = os.path.join(self._dir_, "data/")
        self.support._load_images()

        self.support.set_rewards()

        self.backdrop = None
        self.frog = None

    def setHuman(self, humanAct):
        self.humanAction = humanAct

    def init(self):
        self.backdrop = Backdrop(self.width,self.height,self.images["background"])

        # frog init
        self.frog = Frog(kPlayFrog, self.images["frog"]["stationary"])

        # place homes
        self.support.init_homes()
        self.numFrogsHomed=0

        self.support.init_cars()
        self.support.init_river()

        self.mapCars()
        self.mapRivers()

        self.bonusCroc=BonusRandom(kCrocodileBonusRate,kCrocodileBonusDelay)
        self.bonusFly=BonusRandom(kFlyBonusRate,kFlyBonusDelay)

        self.reachedMidway = False

        self.score = 0.0
        self.lives = 1
        self.game_tick = 0

        self.term=False

        self.humanWaiting = False
        self.human = False
        self.testingRun=False # pretty much will always be this unless we actually are testing
        self.show=True

    def mapCars(self):
        self.carMap={}
        for car in self.cars:
            self.carMap[car.getLabel()] = car

    def mapRivers(self):
        self.riverMap={}
        for river in self.river_group.sprites():
            self.riverMap[river.getLabel()] = river

    def getLocOfSpriteAtLabel(self, label):
        if label < 16: return self.riverMap[label].get_pos()
        else: return self.carMap[label].get_pos()            

    def getScore(self):
        return self.score

    def getGameState(self):
        homeStatus=[None]*len(self.homes)
        for i in range(len(self.homes)):
            if self.homes[i].frogged == True: homeStatus[i] = float(0.66) # fly is worth more
            elif self.homes[i].flied == True: homeStatus[i] = float(1)
            elif self.homes[i].croced == True: homeStatus[i] = float(0.33) # let's frog know what's here
            else: homeStatus[i] = 0

        state = {
            'frog_x': self.frog.get_pos()[0],
            'frog_y': self.frog.get_pos()[1],
            'rect_w': self.frog.get_rect().width,
            'rect_h': self.frog.get_rect().height,
            'cars': self.support.carRects(),
            #'rivers': self.support.riverRects(),
            'riverS': self.river_group.sprites(),
            'homes': homeStatus,
            'homeR': self.support.homeRects(),
            'frog': self.frog,
            'term': self.term,
            'carInf': self.cars,
            'riverInf': self.river_group.sprites(),
            'homeInf': self.homes
        }

        ''' for testing:
        ,
        'frog':self.frog,
        'riverS':self.river_group.sprites()'''

        if self.term: self.term=False

        #return state
        return state,self.human
        '''if self.nextActionFromHuman:
            self.nextActionFromHuman = False
            return state,True
        else:
            return state,False'''

    def _handle_player_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                key = event.key
                if key == self.actions["up"]:
                    self.frog.set_move((0.0,-1.0))
                if key == self.actions["right"]:
                    self.frog.set_move((1.0,0.0))
                if key == self.actions["down"]:
                    self.frog.set_move((0.0,1.0))
                if key == self.actions["left"]:
                    self.frog.set_move((-1.0,0.0))
                if key == K_SPACE and self.testingRun==False:
                    if self.human and self.humanWaiting:
                        self.humanAction.nextAction = K_F15
                        continue
                    if self.human:
                        self.human=False
                    else:
                        self.human=True
                        self.clock=None
                        self.humanWaiting=True
                        self.warped=False
                if key == K_UP and self.human:
                    self.humanAction.nextAction = K_EXCLAIM
                if key == K_DOWN and self.human:
                    self.humanAction.nextAction = K_DOLLAR
                if key == K_LEFT and self.human:
                    self.humanAction.nextAction = K_AMPERSAND
                if key == K_RIGHT and self.human:
                    self.humanAction.nextAction = K_HASH

                    '''self.clock=None
                    self.nextActionFromHuman=True'''

    def game_over(self):
        if self.numFrogsHomed == 5:
            # game over because we won
            return True
        return self.lives <= 0

    def getVector(self, initPos, nextPos):
        return ( ((nextPos[0] - initPos[0])/kPlayCellSize[0]),
                 ((nextPos[1] - initPos[1])/kPlayCellSize[1]) )

    def timeWarpStep(self, locations):
        # update position of all non-frog sprites, starting with river
        for i in range(16):
            self.riverMap[i].set_pos(locations[i])
        # cars
        for i in range(16,27):
            self.carMap[i].set_pos(locations[i])
        # frog
        # setting move, just need to calculate the vector for this transition
        vec=self.getVector(self.frog.get_pos(), locations[27][0])
        self.frog.set_move(vec)
        if vec[1] == 12.0 and (self.numFrogsHomed+1) == locations[33]:
            # homed a frog on this movement, need to update the midway track
            self.reachedMidway=False
        
        # homes        
        for i in range(len(self.homes)):
            if locations[30][i][0] == True:
                # frog in the home, set frog in this home for the warp
                self.homes[i].homeFrog()
                continue
            else:
                # not supposed to have a frog, if it does, get rid of it
                if self.homes[i].frogged:
                    self.homes[i].frogged=False
                    self.homes[i].image = self.homes[i].image_assets["blank"]
                    continue
            if locations[30][i][1] != None:
                if locations[30][i][1][0] == 'croc':
                    self.homes[i].setCroc()
                    self.homes[i].duration = locations[30][i][1][1]
                    continue
                if locations[30][i][1][0] == 'fly':
                    self.homes[i].setFly()
                    self.homes[i].duration = locations[30][i][1][1]
                    continue
            else:
                self.homes[i].duration = kHomeBonusDuration+1
                
        self.numFrogsHomed=locations[33]
        self.step(0)
        # update the direction to mkae it look normal on backwards movement
        self.frog.direction=locations[27][1]
        self.frog.update_image()
        if self.show:
            self.drawUpdates()
            
    def drawUpdates(self):
        self.backdrop.draw_background(self.screen)
        for home in self.homes: home.draw(self.screen)
        for car in self.cars: car.draw(self.screen)
        self.river_group.draw(self.screen)
        self.frog.draw(self.screen)
        self.backdrop.draw_outerEdge(self.screen)
        self.backdrop.draw_budgetMessage(self.humanAction.n)
        self.backdrop.draw_messageCenter(self.humanAction.messages)
        pygame.display.update()

    def step(self, dt):
        self.game_tick += 1
        #dt = dt / 1000.0

        self._handle_player_events()
        #if self.humanWaiting and dt > 0: return

        # all sprites do their update
        for home in self.homes: home.update(dt)
        for car in self.cars: car.update(dt)
        self.river_group.update(dt)
        self.frog.update(dt)

        # check for collisions with the homes first
        if self.frog._y < kPlayYHomeLimit:
            collideInd = self.frog.get_rect().collidelist(self.homeRects)
            if collideInd != -1 and self.homes[collideInd].frogged == False and self.homes[collideInd].croced == False:
                # hit an open home, kill frog and init a new one
                if self.homes[collideInd].flied == True:
                    # fly bonus --> give reward here
                    print 'fly bonus!'
                self.homes[collideInd].homeFrog()
                if not self.humanWaiting: self.score += self.rewards['home']
                self.numFrogsHomed += 1
                if self.numFrogsHomed == 5 and not self.humanWaiting: self.score += self.rewards['win']
                self.frog.kill()
                self.term=True
                self.frog = Frog(kPlayFrog, self.images["frog"]["stationary"])
                self.reachedMidway = False
            else:
                self.lives -= 1
                if not self.humanWaiting: self.score += self.rewards['death']
                self.term=True
        elif self.frog._y < kPlayYRiverLimit:
            # in the river zone
            h=pygame.sprite.spritecollide(self.frog,self.river_group,False)
            if len(h) == 0:
                # fell in the river
                self.lives -= 1
                if not self.humanWaiting: self.score += self.rewards['death']
                self.term=True
            else:
                # if haven't been given the midway reward, give it now
                if self.reachedMidway == False:
                    if not self.humanWaiting: self.score += self.rewards['midway']
                    self.reachedMidway = True
                # attach to h[0] (first check that we aren't hopping laterally)
                if self.frog.attached and h[0] != self.frog.attachedObj:
                    if h[0].get_rect()[1] == self.frog.attachedObj.get_rect()[1]:
                    # tried to move laterally between attached objects, can't do that, kill
                        self.lives -= 1
                        if not self.humanWaiting: self.score += self.rewards['death']
                        self.term=True
                    else:
                        self.frog.attachTo(h[0])
                        if self.frog._y <= kPlayYRiver[0] and self.frog._y >= kPlayYRiver[1]:
                            if not self.humanWaiting: self.score += float(0.1)
                        elif self.frog._y <= kPlayYRiver[1] and self.frog._y >= kPlayYRiver[2]:
                            if not self.humanWaiting: self.score += float(0.2)
                        elif self.frog._y <= kPlayYRiver[3] and self.frog._y >= kPlayYRiver[4]:
                            if not self.humanWaiting: self.score += float(0.4)
                        elif self.frog._y <= kPlayYRiver[2] and self.frog._y >= kPlayYRiver[3]:
                            if not self.humanWaiting: self.score += float(0.3)
                        elif self.frog._y <= kPlayYRiver[4]:
                            if not self.humanWaiting: self.score += float(0.5)
                else:
                    self.frog.attachTo(h[0])

                # check if frog has gone off screen while attached to object, kill
                if self.frog.attachDisappeared():
                    self.lives -= 1
                    if not self.humanWaiting: self.score += self.rewards['death']
                    self.term=True

                # check for diving turtle
                if isinstance(self.frog.attachedObj, Turtle) and self.frog.attachedObj.disappeared == True:
                    # if frog is attached to a disappeared Turtle, kill
                    '''self.lives -= 1
                    self.score += self.rewards['death']
                    self.term=True'''
                    pass
        else:
            # in the road zone, check for collisions with cars
            # if we have been given the midway reward and went back to the road, need to penalize
            if self.reachedMidway == True:
                if not self.humanWaiting: self.score += self.rewards['downmid']
                #self.reachedMidway = False
            collideInd = self.frog.get_rect().collidelist(self.support.carRects())
            '''if self.frog.attached:
                self.frog.attached = False
                self.frog.attachedObj = None'''
            if collideInd != -1:
                # collided with a car, kill the frog
                self.lives -= 1
                if not self.humanWaiting: self.score += self.rewards['death']
                self.term=True

            # may need to add the car reward
            '''
            if self.frog._y <= kPlayYCar[0] and self.frog._y >= kPlayYCar[1]:
                self.score += float(0.1)
            elif self.frog._y <= kPlayYCar[1] and self.frog._y >= kPlayYCar[2]:
                self.score += float(0.2)
            elif self.frog._y <= kPlayYCar[2] and self.frog._y >= kPlayYCar[3]:
                self.score += float(0.3)
            elif self.frog._y <= kPlayYCar[3] and self.frog._y >= kPlayYCar[4]:
                self.score += float(0.4)
            elif self.frog._y <= kPlayYCar[4]:
                self.score += float(0.5)
            '''

        
        # determine if homes should get a croc/fly placed
        if not self.humanWaiting:
            if self.bonusCroc.get_chance(self.clock.get_time()): self.homes[int(5*random.random())].setCroc()
            if self.bonusFly.get_chance(self.clock.get_time()): self.homes[int(5*random.random())].setFly()

        if self.show: self.drawUpdates()
