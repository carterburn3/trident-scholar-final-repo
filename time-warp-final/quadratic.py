from ple import PLE
import frogger
from constants import *
import numpy as np
import pygame
from pygame.constants import K_F15,K_LEFT,K_RIGHT,K_UP,K_DOWN,K_AMPERSAND,K_HASH,K_DOLLAR,K_EXCLAIM,K_SPACE
import random
from optparse import OptionParser
from features import NonLinearFeatures
from nonlinearagent import NonLinearAgent
import os
import cPickle as pickle
from copy import deepcopy
from time import sleep, time

def parseOptions():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 1.0")
    parser.add_option("-t", "--testing",
                      action="store_true",
                      dest="testing",
                      default=False,
                      help="to enter testing mode")
    parser.add_option("-a", "--alpha",
                      action="store",
                      type="float",
                      dest="alpha",
                      default=float(0.006),
                      help="define an alpha")
    parser.add_option("-e", "--episodes",
                      action="store",
                      type="int",
                      dest="numEpisodes",
                      default=int(20000),
                      help="set number of episodes")
    parser.add_option("-g", "--gamma",
                      action="store",
                      type="float",
                      dest="discount",
                      default=float(0.9),
                      help="set discount factor")
    parser.add_option("-E", "--epsilon",
                      action="store",
                      type="float",
                      dest="epsilon",
                      default=float(0.1),
                      help="set epsilon value")
    parser.add_option("-f", "--testFileNum",
                      action="store",
                      type="int",
                      dest="saveNum",
                      default=int(0),
                      help="set test number")
    parser.add_option("-H", "--threshold",
                      action="store",
                      type="float",
                      dest="thresh",
                      default=float(1.0),
                      help="set threshold for learning agent")
    parser.add_option("-b", "--budget",
                      action="store",
                      type="int",
                      dest="budget",
                      default=int(1000),
                      help="set budget for teacher advice")
    parser.add_option('-N', '--number',
                      action='store',
                      type='str',
                      dest='number',
                      default='000000',
                      help='define alpha number for testing')
    parser.add_option('-w', '--weights',
                      action='store',
                      type='str',
                      dest='weightLoad',
                      default='none',
                      help='define the weights used')
    (options, args) = parser.parse_args()

    return options

def getNextState(n_s):
    if n_s['term']: return None
    else: return n_s

options=parseOptions()
options.number = options.number.replace(' ','')
if (not options.saveNum and options.number == '000000') and not options.testing:
    print 'PLEASE ENTER A TEST NUMBER'
    exit(1)
    
# track the start time of the experiment
startTime = time()
game=frogger.Frogger()
from humanActions import HumanActions
humanAct=HumanActions(game.images["background"], options.budget)
humanAct.setGame(game)
game.setHuman(humanAct)
fps=30
p=PLE(game,fps=fps,force_fps=False,add_noop_action=True,display_screen=True)
agent=NonLinearAgent(p.getActionSet(),None,options.testing,options.thresh,options.budget,alpha=options.alpha,epsilon=options.epsilon,gamma=options.discount)
# load up the correct weights
if options.weightLoad == '1000':
    # load up 1000 epoch weights
    fi=open('/home/mids/m180732/experiment-files/epi-1000')
    agent.weights=pickle.load(fi)
    fi.close()
    print '1000 epoch weights being loaded'
elif options.weightLoad == '10000':
    # load up 10000 epoch weights
    fi=open('/home/mids/m180732/experiment-files/epi-10000')
    agent.weights=pickle.load(fi)
    fi.close()
    print '10000 epoch weights being loaded'
else: pass

# otherwise, the agent will have loaded up blank weights on initializing the agent
agent.printOut=False
reward=0.0
s,human=p.getGameState()
midDecay=options.numEpisodes/2
np.set_printoptions(linewidth=300)
test_list=[]
cumm_attn=[]
printqs=False
attnDur=-1
statesSeen=1

def actionVector(action):
    if action == K_EXCLAIM:   return (0.0,-1.0)
    elif action == K_HASH: return (1.0,0.0)
    elif action == K_DOLLAR: return (0.0,1.0)
    elif action == K_AMPERSAND: return (-1.0,0.0)
    else: return (0.0,0.0)

def updateSavedStates(savedStates, deathEpisode):
    if deathEpisode == 1: return savedStates
    # if not the first episode, going to go find the first time
    # deathEpisode occurs in saved and trim from there
    indOfFirst=-1
    for i,save in enumerate(savedStates):
        if save[28] == deathEpisode:
            # found the first index of this episode
            indOfFirst=i
            break
    # return the trimmed array from the indOfFirst
    return savedStates[indOfFirst:]

def saveState(game, epi, wts, tC, sS):
    locs=[None]*34
    for i in range(27):
        locs[i] = game.getLocOfSpriteAtLabel(i)

    locs[27] = (game.frog.get_pos(),game.frog.direction)
    locs[28] = epi
    locs[29] = wts
    homeInfo=[None]*5
    for i in range(len(game.homes)):
        if game.homes[i].frogged:
            homeInfo[i] = (True, None)
        elif game.homes[i].croced:
            homeInfo[i] = (False, ('croc', deepcopy(game.homes[i].duration)))
        elif game.homes[i].flied:
            homeInfo[i] = (False, ('fly', deepcopy(game.homes[i].duration)))
        else:
            homeInfo[i] = (False, None)
    locs[30] = homeInfo
    locs[31] = tC
    locs[32] = sS
    locs[33] = game.numFrogsHomed
    return locs

path=None
# all files relative the path
# if an alpha is given, assuming on michelson lab machine and we need to use the alpha!
if options.number != '000000':
    path='/home/mids/m180732/experiment-results/'+str(options.number)+'/'
else:
    path='./testRes/test'+str(options.saveNum)+'/'

# create directory to save results in
if not os.path.exists(path):
    try: os.makedirs(path)
    except:
        print 'ERROR CREATING TEST DIRECTORY'
        exit(1)


#fi=open(path+'experiment','w')
#fi.write('thres='+str(options.thresh)+' budget='+str(options.budget)+' alpha='+str(options.alpha)
#         +' epsilon='+str(options.epsilon)+' gamma='+str(options.discount)+' epi='
#         +str(options.numEpisodes))
#fi.close()
# experiment saving is done with the launch.py!

def save_weights(weights,epi):
    if not os.path.exists(path+'mid-weight-save/'):
        try: os.makedirs(path+'mid-weight-save/')
        except:
            print 'ERROR CREATING WEIGHT SAVE DIRECTORY'
            exit(1)
    s=path+'mid-weight-save/epi-'+str(epi)
    fi=open(s,'w')
    pickle.dump(weights,fi)
    fi.close()

def endHumanExp(agent, human, sS, episode, cA, aD, path):
    fi=open(path+'endHuman','w')
    info={}
    info['weights'] = agent.weights
    info['budget'] = human.n
    info['teacherCount'] = human.teacherCount
    info['statesSeen']=sS
    info['episode']=episode
    info['cumm_attn'] = cA
    info['attnDur'] = aD
    info['epsilon'] = agent.epsilon
    pickle.dump(info, fi)
    fi.close()

savedStates=[]

episode = 1
while episode < options.numEpisodes+1:
    print 'BEGINNING EPISODE ' + str(episode)

    # check if the time limit has been reached
    if int(time() - startTime) > 600:
        # 1200 is 20 minutes
        endHumanExp(agent, humanAct, statesSeen, episode, cumm_attn, attnDur, path)
        humanAct.showExpOver()
        exit()
        
    printqs=False
    agent.printOut=False

    if episode >= midDecay:
        agent.epsilon = agent.epsilon * 0.5
        midDecay += (midDecay*0.5)

    if os.path.isfile('./print-weights'):
        print agent.weights
    if os.path.isfile('./print-qs'):
        printqs=True
    if os.path.isfile('./print-out'):
        agent.printOut=True

    while True:
        if p.game_over():
            # first save the death, then update the savedStates
            savedStates.append(saveState(game, episode, None, humanAct.teacherCount, statesSeen))
            savedStates=updateSavedStates(savedStates, episode)
            break

        # save the state
        savedStates.append(saveState(game, episode, deepcopy(agent.weights), humanAct.teacherCount, statesSeen))
        
        action=None
        if human and humanAct.n > 0:
            if not game.warped:
                game.warped = True
                # save the weights that the human wants to use
                retVal=humanAct.timeWarp(savedStates)
                if retVal == None:
                    endHumanExp(agent,humanAct,statesSeen,episode,cumm_attn,attnDur,path)
                    humanAct.showExpOver()
                    exit()
                episode=retVal[0]
                retWeights=retVal[1]
                # save the weights
                agent.weights = retWeights
                # reset the statesSeen and teacher count
                humanAct.teacherCount=retVal[2]
                statesSeen = retVal[3]
                #print statesSeen, humanAct.teacherCount
                # get the new state
                s,human=p.getGameState()
                savedStates=[]
                savedStates.append(saveState(game, episode, deepcopy(agent.weights), humanAct.teacherCount, statesSeen))
            action=humanAct.getActionFromHuman()
            
        else:
            if game.human: game.human=False
            humanAct.messages=['Agent Training']
            action=agent.pickAction(s,printqs,episode)
        reward=p.act(action)
        #print reward
        statesSeen+=1 # recording all of the states we've seen thus far
        next_s,human=p.getGameState()
        n_s=getNextState(next_s)
        agent.update(s,action,n_s,reward)
        s=n_s
        if s==None: s = next_s

    p.reset_game()
    # check if advice budget has ran out in this episode
    if humanAct.n == 0 and attnDur == -1: attnDur = episode
    # save this episode's reward to the reward list
    #rewards_list.append( (int(episode), totalReward))
    if episode%100==0:
        # just saving the weights for each 100 episodes for testing later
        save_weights(agent.weights, episode)
        # if cumm attn list is empty changes the calc
        if len(cumm_attn) == 0:
            cumm_attn.append((int(episode), float(humanAct.teacherCount/float(statesSeen))))
            # reset count for next 100 episodes
            statesSeen=1
            humanAct.teacherCount=0
        else:
            if cumm_attn[-1][0] == episode:
                # already exists
                if len(cumm_attn) == 1:
                    cumm_attn[-1] = (int(episode),float(humanAct.teacherCount/float(statesSeen)))
                else:
                    val=cumm_attn[-2][1]+float(humanAct.teacherCount/float(statesSeen))
                    cumm_attn[-1] = (int(episode), val)
            else:
                # doesn't exist, just append
                val=cumm_attn[-1][1]+float(agent.teacherCount/float(statesSeen))
                cumm_attn.append((int(episode), val))
            statesSeen=1
            humanAct.teacherCount=0    
    if episode%1000==0: save_weights(agent.weights,episode) # will overwrite if we changed back
            
    s,human=p.getGameState()
    episode += 1

fi=open(path+'test-res','w')
pickle.dump(test_list,fi)
fi.close()

fi=open(path+'usage-res','w')
pickle.dump(cumm_attn,fi)
fi.close()

fi=open(path+'attnDur','w')
pickle.dump(attnDur,fi)
fi.close()

'''
fi=open(path+'save-weights-'+str(options.saveNum),'w')
pickle.dump(agent.weights,fi)
fi.close()

# save the test list
fi=open(path+'save-tests-'+str(options.saveNum),'w')
pickle.dump(test_list,fi)
fi.close()
        
# save teacher usage
fi=open(path+'save-usage-'+str(options.saveNum),'w')
pickle.dump(agent.teacherUsage,fi)
fi.close()

# save all the rewards from each episode
fi=open(path+'save-rewards-'+str(options.saveNum),'w')
pickle.dump(rewards_list,fi)
fi.close()
'''
