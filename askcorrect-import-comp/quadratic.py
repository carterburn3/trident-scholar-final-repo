from ple import PLE
import frogger
from constants import *
import numpy as np
import pygame
from pygame.constants import K_F15,K_LEFT,K_RIGHT,K_UP,K_DOWN,K_a,K_d,K_s,K_w
import random
from optparse import OptionParser
from features import NonLinearFeatures
import os
import cPickle as pickle

def parseOptions():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 1.0")
    parser.add_option("-t", "--testing",
                      action="store_true",
                      dest="testing",
                      default=False,
                      help="to enter testing mode")
    parser.add_option("-a", "--alpha",
                      action="store",
                      type="float",
                      dest="alpha",
                      default=float(0.006),
                      help="define an alpha")
    parser.add_option("-e", "--episodes",
                      action="store",
                      type="int",
                      dest="numEpisodes",
                      default=int(20000),
                      help="set number of episodes")
    parser.add_option("-g", "--gamma",
                      action="store",
                      type="float",
                      dest="discount",
                      default=float(0.9),
                      help="set discount factor")
    parser.add_option("-E", "--epsilon",
                      action="store",
                      type="float",
                      dest="epsilon",
                      default=float(0.1),
                      help="set epsilon value")
    parser.add_option("-f", "--testFileNum",
                      action="store",
                      type="int",
                      dest="saveNum",
                      default=int(0),
                      help="set test number")
    parser.add_option("-H", "--threshold",
                      action="store",
                      type="float",
                      dest="thresh",
                      default=float(1.0),
                      help="set threshold for learning agent")
    parser.add_option("-b", "--budget",
                      action="store",
                      type="int",
                      dest="budget",
                      default=int(1000),
                      help="set budget for teacher advice")
    parser.add_option("-s", "--studThresh", action="store", type="float", dest="studThresh",default=float(1.0), help="set threshold for ask important")
    parser.add_option('-p', '--path', action='store', type='int', dest='exp', default=int(0), help='set exp num')
    (options, args) = parser.parse_args()

    return options

class NonLinearAgent:
    def __init__(self, actions, features, testing, threshold, budget, studT, alpha=0.006, epsilon=0.04, gamma=0.95):
        self.actions=actions
        self.features=NonLinearFeatures()
        self.alpha=alpha
        self.epsilon=epsilon
        self.discount=gamma
        self.printOut=False
        self.testing=testing
        self.weights=None
        self.teachWeights=None
        if not self.testing:
            self.weights=[self.features.initializeWeights() for x in range(len(self.actions))]
            self.teachWeights=self.loadTeacherWeights()
        else:
            self.weights=self.features.loadWeights()

        # teacher information
        self.n=int(budget)
        self.threshold=threshold
        self.studT=studT
        self.teacherCount=0

    def loadTeacherWeights(self):
        teach=None
        fi=open('/mnt/lustre/scratch/m180732/askcorrect-import-comp/teacher-weights')
        print 'opening teacher-weights...'
        teach=pickle.load(fi)
        fi.close()
        return teach
            
    def featsMatVec(self, feats):
        # takes the features dictionary and creates np matrices/vectors for the math
        featsMat=np.zeros((16,16))
        featsVec=np.zeros((16,1))
        for key in feats.keys():
            if key >= 0 and key < 16:
                # add to our matrix
                featsMat[int(key)][16-(16-int(key)):] = feats[key]
            if key >= 1 and key < 17:
                featsVec[int(key)-1][0] = feats[key]

        # squares of each weight
        sqrVec=np.zeros((1,17))
        for key in feats.keys():
            if key >= 0 and key < 17:
                sqrVec[0][int(key)]=feats[key]*feats[key]

        return featsMat,featsVec,sqrVec

    def linFeats(self, feats):
        linFeats=np.zeros((17,1))
        for key in feats.keys():
            linFeats[key][0] = feats[key]
        return linFeats

    def switchAction(self, action):
        # returns the row number of the weights matrix for this particular action
        if action == K_w:
            return 0
        elif action == K_d:
            return 1
        elif action == K_s:
            return 2
        elif action == K_a:
            return 3
        else:
            # action is NOOP
            return 4

    def getQValue(self, state, action):
        # calculates the Q-value of a the state,action pair
        qval=0.0
        feats=self.features.getFeatures(state)
        # get the matrix representations of the features
        featsMat,featsVec,sqrVec = self.featsMatVec(feats)
        featsLin=self.linFeats(feats)
        
        # get the weights for this action (bilinear and sqr)
        weightsBin=self.weights[self.switchAction(action)][0]
        weightsSqr=self.weights[self.switchAction(action)][1]
        weightsLin=self.weights[self.switchAction(action)][2]

        # inner product of the bilinear
        res=np.dot((weightsBin*featsMat),featsVec)
        # sum of all the values adds to the qval
        qval += sum(res)[0]

        # inner product of the squared weights and features
        res=np.dot(sqrVec,weightsSqr)
        # sum of all of these values add to the qval
        qval += sum(res)[0]

        # inner product of the linear weights and features
        qval+=np.dot(weightsLin,featsLin)[0][0]

        return qval

    def getTeacherQValue(self, state, action):
        qval=0.0
        feats=self.features.getFeatures(state)
        featsMat,featsVec,sqrVec=self.featsMatVec(feats)
        featsLin=self.linFeats(feats)

        # get teacher weights for this action
        weightsBin=self.teachWeights[self.switchAction(action)][0]
        weightsSqr=self.teachWeights[self.switchAction(action)][1]
        weightsLin=self.teachWeights[self.switchAction(action)][2]

        #bilinear
        res=np.dot((weightsBin*featsMat),featsVec)
        qval+=sum(res)[0]

        # sqr
        res=np.dot(sqrVec,weightsSqr)
        qval+=sum(res)[0]

        #linear
        qval+=np.dot(weightsLin,featsLin)[0][0]

        return qval

    def getLegalActions(self, state):
        # returns a list of all possible actions for the agent, mostly just gets rid
        # of any actions that would force the agent off of the screen
        possAct = [x for x in self.actions]
        for act in possAct:
            if act != K_F15:
                # leaving NOOP always
                vector=(0.0,0.0)
                if act == K_LEFT: vector = (-1.0,0.0)
                elif act == K_RIGHT: vector = (1.0,0.0)
                elif act == K_UP: vector = (0.0,-1.0)
                else: vector = (0.0,1.0)
                (x,y)=(state['frog_x'],state['frog_y'])
                x+=kPlayCellSize[0]*vector[0]
                y+=kPlayCellSize[1]*vector[1]
                if act == K_LEFT and ((x+state['rect_w']) < 0.0): possAct.remove(K_LEFT)
                if act == K_RIGHT and ((x+state['rect_w']) > kPlayWidth): possAct.remove(K_RIGHT)
                if act == K_UP and ((y+state['rect_h']) < 0.0): possAct.remove(K_UP)
                if act == K_DOWN and ((y+state['rect_h']) > kPlayHeight-kPlayCellSize[1]): possAct.remove(K_DOWN)

        return possAct

    def getValue(self, state):
        # returns the value of a state, if none, it's a terminal state with value of 0
        if state == None:
            print 'terminal state!'
            return 0.0 # terminal state
        # list of all qvals for every legal action in the state
        qvals=[ self.getQValue(state,action) for action in self.getLegalActions(state)]
        # return the max of the qvals
        return max(qvals)

    def getTrainingPolicy(self, state, printqs):
        # return the argmax_a of Q(s,a), unless they are equal, then return a random action
        qvals=[(self.getQValue(state,action),action) for action in self.getLegalActions(state)]
        if len(set(qvals)) <= 1:
            # all equal, return random action
            print 'random action'
            return random.choice(self.getLegalActions(state))
        else:
            # if we're printing the qvals, then we print out the max
            if printqs: print max(qvals)[0]
            return max(qvals)[1]

    def getPolicy(self, state):
        # only returns the argmax_a Q(s,a)
        qvals=[(self.getQValue(state,action),action) for action in self.getLegalActions(state)]
        return max(qvals)[1]

    def getTeacherAction(self, state):
        qvals=[(self.getTeacherQValue(state,action),action) for action in self.getLegalActions(state)]
        return max(qvals)[1]

    def calcTeachImportance(self, state):
        qvals=[ (self.getTeacherQValue(state,action)) for action in self.getLegalActions(state)]
        return max(qvals)-min(qvals)

    def calcStudImportance(self, state):
        qvals=[ (self.getQValue(state,action)) for action in self.getLegalActions(state)]
        return max(qvals)-min(qvals)

    def pickAction(self, state, pqs, episode):
        if self.testing: return self.getPolicy(state)
        else:
            # first get student's desired action
            studAct=None
            if random.random() < self.epsilon: studAct=random.choice(self.getLegalActions(state))
            else: studAct=self.getTrainingPolicy(state,pqs)
            # next the student will calculate importance of state 
            if self.calcStudImportance(state) >= self.studT:
                if self.n > 0 and self.calcTeachImportance(state) >= self.threshold:
                    teachAct=self.getTeacherAction(state)
                    if studAct != teachAct:
                        self.n -= 1
                        print 'Teacher advised action'
                        self.teacherCount+=1
                        return teachAct
            return studAct
        
    def update(self, state, action, nextState, reward):
        # only update if we are not testing
        if not self.testing:
            # get the matrix representation of the features and the weights for this action
            feats=self.features.getFeatures(state)

            featsMat,featsVec,sqrVec = self.featsMatVec(feats)

            weightsBin=self.weights[self.switchAction(action)][0]
            weightsSqr=self.weights[self.switchAction(action)][1]
            weightsLin=self.weights[self.switchAction(action)][2]

            # calculate V(s), Q(s,a) and the correction r+\gamma*V(s) - Q(s,a)
            value=self.getValue(nextState)
            cur=self.getQValue(state,action)
            correction=(reward+(self.discount*value))-cur

            if self.printOut:
                print 'Before Update: Feats:',feats,'Weights:',weightsBin,weightsSqr,weightsLin,'a:',self.alpha,'g:',self.discount,'r:',reward,'Next State:',value,'Q:',cur

            # update the bilinear features deriv according to update rule:
            # w = w + \alpha * correction * deriv (w/ respect to the weight)
            for i in range(16):
                for j in range(16):
                    if j >= i:
                        deriv=featsMat[i][i]*featsVec[j][0]
                        weightsBin[i][j] += float(self.alpha*correction*deriv)

            # update the squared weights
            for i in range(17):
                weightsSqr[i][0] += float(self.alpha * correction * (feats[i]*feats[i]))

            # update the linear weights
            for i in range(17):
                weightsLin[0][i] += float(self.alpha * correction * feats[i])

            # replace the updated weights
            self.weights[self.switchAction(action)] = (weightsBin,weightsSqr,weightsLin)

            if self.printOut:
                print 'After Update: Feats:',feats,'Weights:',self.weights[self.switchAction(action)][0],self.weights[self.switchAction(action)][1],self.weights[self.switchAction(action)][2]

def getNextState(n_s):
    if n_s['term']: return None
    else: return n_s

def runTest(agent,p):
    total=0.0
    for i in range(30):
        p.reset_game()
        s=p.getGameState()
        print 'TEST',i
        badStates=0
        while True:
            if p.game_over() or badStates == 100:
                break
            if s['frog_y'] > 430.0: badStates += 1
            else: badStates = 0
            action=agent.getPolicy(s)
            total+=p.act(action)
            s=p.getGameState()
    p.reset_game()
    return float(total/30.0)

options=parseOptions()
if not options.saveNum and not options.testing:
    print 'PLEASE ENTER A TEST NUMBER'
    exit(1)
game=frogger.Frogger()
fps=30
p=PLE(game,fps=fps,force_fps=False,add_noop_action=True,display_screen=True)
agent=NonLinearAgent(p.getActionSet(),None,options.testing,options.thresh,options.budget,options.studThresh,alpha=options.alpha,epsilon=options.epsilon,gamma=options.discount)
agent.printOut=False
reward=0.0
s=p.getGameState()
reachedMid=0
midDecay=options.numEpisodes/2
np.set_printoptions(linewidth=300)
test_list=[]
cumm_attn=[]
printqs=False
attnDur=-1
statesSeen=1

#path='./testRes/test'+str(options.saveNum)+'/'
base='/mnt/lustre/scratch/m180732/askcorrect-import-comp/testRes/exp'+str(options.exp)+'/'
path=base+'test'+str(options.saveNum)+'/'
# create directory to save results in
if not os.path.exists(path):
    try: 
        os.makedirs(path)
        print 'making',path
    except:
        print 'ERROR CREATING TEST DIRECTORY'
        exit(1)

#path='./testRes/test'+str(options.saveNum)+'/'

fi=open(path+'experiment','w')
fi.write('thres='+str(options.thresh)+' budget='+str(options.budget)+' alpha='+str(options.alpha)
         +' epsilon='+str(options.epsilon)+' gamma='+str(options.discount)+' epi='
         +str(options.numEpisodes)+' stud thresh='+str(options.studThresh))
fi.close()

def save_weights(weights,epi):
    if not os.path.exists(path+'mid-weight-save/'):
        try: os.makedirs(path+'mid-weight-save/')
        except:
            print 'ERROR CREATING WEIGHT SAVE DIRECTORY'
            exit(1)
    s=path+'mid-weight-save/epi-'+str(epi)
    fi=open(s,'w')
    pickle.dump(weights,fi)
    fi.close()

for episode in range(1, options.numEpisodes+1):
    print 'BEGINNING EPISODE ' + str(episode)

    printqs=False
    agent.printOut=False
    
    if episode >= midDecay:
        agent.epsilon = agent.epsilon * 0.5
        midDecay += (midDecay*0.5)

    if os.path.isfile('/mnt/lustre/scratch/m180732/askcorrect-import-comp/print-weights'):
        print agent.weights
    if os.path.isfile('/mnt/lustre/scratch/m180732/askcorrect-import-comp/print-qs'):
        printqs=True
    if os.path.isfile('/mnt/lustre/scratch/m180732/askcorrect-import-comp/print-out'):
        agent.printOut=True

    while True:
        if p.game_over():
            break

        action=agent.pickAction(s, printqs, episode)
        reward=p.act(action)
        statesSeen+=1 # recording all of the states we've seen thus far
        next_s=p.getGameState()
        n_s=getNextState(next_s)
        agent.update(s,action,n_s,reward)
        s=n_s
        if s==None: s = next_s

    p.reset_game()
    # check if advice budget has ran out in this episode
    if agent.n == 0 and attnDur == -1: attnDur = episode
    # save this episode's reward to the reward list
    #rewards_list.append( (int(episode), totalReward))
    if episode%100==0:
        # run the test
        test_list.append( (int(episode), runTest(agent,p)))
        # if cumm attn list is empty changes the calc
        if len(cumm_attn) == 0:
            cumm_attn.append((int(episode), float(agent.teacherCount/float(statesSeen))))
            # reset count for next 100 episodes
            statesSeen=0
            agent.teacherCount=0
        else:
            val=cumm_attn[-1][1]+float(agent.teacherCount/float(statesSeen))
            cumm_attn.append((int(episode), val))
            statesSeen=0
            agent.teacherCount=0
    if episode%1000==0: save_weights(agent.weights,episode)
    s=p.getGameState()
    
    
fi=open(path+'test-res','w')
pickle.dump(test_list,fi)
fi.close()

fi=open(path+'usage-res','w')
pickle.dump(cumm_attn,fi)
fi.close()

fi=open(path+'attnDur','w')
pickle.dump(attnDur,fi)
fi.close()

'''
fi=open(path+'save-weights-'+str(options.saveNum),'w')
pickle.dump(agent.weights,fi)
fi.close()

# save the test list
fi=open(path+'save-tests-'+str(options.saveNum),'w')
pickle.dump(test_list,fi)
fi.close()
        
# save teacher usage
fi=open(path+'save-usage-'+str(options.saveNum),'w')
pickle.dump(agent.teacherUsage,fi)
fi.close()

# save all the rewards from each episode
fi=open(path+'save-rewards-'+str(options.saveNum),'w')
pickle.dump(rewards_list,fi)
fi.close()
'''
