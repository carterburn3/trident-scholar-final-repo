import os

def submitJob(exp, tests):
    s=''
    for item in tests:
        s=s+str(item)+'-'
    s=s[:-1]

    os.environ["EX"]=exp
    os.environ["JB"]=s
    teachT=None
    budg=None
    studT=None
    if exp == '1':
        teachT='12.0'
        budg='5000'
        studT='9.0'
    elif exp == '2':
        teachT='12.0'
        budg='5000'
        studT='10.0'
    elif exp == '3':
        teachT='12.0'
        budg='3000'
        studT='9.0'
    else:
        teachT='12.0'
        budg='3000'
        studT='10.0'
    os.environ["TT"]=teachT
    os.environ["BD"]=budg
    os.environ["ST"]=studT
    os.system("qsub -v TEACHT=$TT,BUDG=$BD,STUDT=$ST,EXPN=$EX,JOBS=$JB /mnt/lustre/scratch/m180732/advise-import-comp/job.sh")
    print 'submitted',exp,tests


tests={'3':[17,21],
       '4':[17,27]}

jobList={}
for exp in tests:
    if len(tests[exp]) > 15:
        split=len(tests[exp])/2+1
        jobList[exp]=[tests[exp][:split],tests[exp][split:]]
    else:
        jobList[exp]=[tests[exp][:]]

exps=jobList.keys()

for i in range(len(exps)):
    submitJob(exps[i], jobList[exps[i]][0])
    if len(jobList[exps[i]]) == 2: submitJob(exps[i],jobList[exps[i]][1])

print 'all done'
