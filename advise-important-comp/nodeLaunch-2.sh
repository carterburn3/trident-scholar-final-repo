#!/bin/bash

killall Xvfb

DISPLAYS=()
XPIDS=()
for i in {101..115};
do
    /usr/bin/Xvfb :$i -nolisten inet6 &
    XPIDS+=($!)
    DISPLAYS+=(:$i)
done

IFS='-'; set -f
jobList=($5)
echo ${jobList[@]}
jobLen=${#jobList[@]}

PIDS=()
for (( i=0; i<jobLen; i++ ))
do
    echo "run:" $4 ${jobList[$i]} "DIS" ${DISPLAYS[$i]}
    export DISPLAY=${DISPLAYS[$i]}
    sleep 2
    python /mnt/lustre/scratch/m180732/advise-import-comp/quadratic.py -a 0.006 -e 5000 -E 0.04 -g 0.95 -H $1 -b $2 -s $3 -f ${jobList[$i]} -p $4 &
    last=?!
    sleep 2
    p=$(ps axu | grep $last | wc -l)
    while [ $p -eq 1 ]
    do
	echo "Died on" ${jobList[$i]}
	kill -9 ${XPIDS[$i]}
	sleep 1
	/usr/bin/Xvfb ${DISPLAYS[$i]} -nolisten inet6 &
	export DISPLAY=${DISPLAYS[$i]}
	sleep 2
	python /mnt/lustre/scratch/m180732/advise-import-comp/quadratic.py -a 0.006 -e 5000 -E 0.04 -g 0.95 -H $1 -b $2 -s $3 -f ${jobList[$i]} -p $4 &
	last=$!
	sleep 2
	p=$(ps axu | grep $last | wc -l)
    done
    PIDS+=($last)
done

sleep 10
echo "PIDS" "${PIDS[@]/#/}"
ps axu | grep python
wait ${PIDS[@]}

killall Xvfb
