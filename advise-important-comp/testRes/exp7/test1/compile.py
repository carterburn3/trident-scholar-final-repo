import cPickle as pickle
import matplotlib.pyplot as plt
import numpy as np

fi=open('./test-res')
tests=pickle.load(fi)
fi.close()

eps,rews=np.array(tests).T
plt.plot(eps,rews,'r',alpha=0.3)
plt.show()
