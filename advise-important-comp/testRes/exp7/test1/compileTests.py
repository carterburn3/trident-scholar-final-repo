from optparse import OptionParser
import cPickle as pickle
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os
import numpy as np

def parseOptions():
    parser = OptionParser(usage="usage: %prog [options]", version="%prog 1.0")
    parser.add_option("-t", "--tests", action="store", type="int", dest="num",default=int(0),help="number of tests")
    (options,args) = parser.parse_args()

    return options

opts=parseOptions()
if not opts.num:
    print 'Please input number of tests to calculate'
    exit(1)

def arrayOfData(filename):
    points={}
    for i in range(1,opts.num+1):
        if os.path.exists('./testRes/test'+str(i)):
            fi=open('./testRes/test'+str(i)+'/'+filename)
            # load into points
            points[i]=pickle.load(fi)
            fi.close()

    # make a totals list of the points
    totals={}
    for key in points:
        for data in points[key]:
            if data[0] in totals:
                totals[data[0]] += data[1]
            else:
                totals[data[0]] = data[1]

    # divide each total by num tests
    for key in totals: totals[key] /= opts.num

    # make array
    data_list=[]
    for key in totals:
        data_list.append( (int(key), totals[key]))

    # sort the data list according to the episode number
    data_list.sort(key=lambda item: item[0])

    return data_list

# get rewards list
test_list = arrayOfData('test-res')
# avg attn dur
total=0
for i in range(1,opts.num+1):
    fi=open('./testRes/test'+str(i)+'/attnDur')
    total+=pickle.load(fi)
    fi.close()
attnDur=int(float(total/float(opts.num)))
print 'ATTENTION DURATION:',attnDur
cumm_attn=arrayOfData('usage-res')

# ready to graph all of it
s='./testRes/graph-avg-results.pdf'
fi=open('./testRes/attnDur','w')
fi.write('ATTENTION DURATION ' + str(attnDur))
fi.close()
pp=PdfPages(s)

# average usage
eps,usg=np.array(cumm_attn).T
plt.plot(eps,usg,'b',alpha=0.3)
plt.title('Cumulative Attention')
plt.xlabel('Episode')
plt.ylabel('Cumulative Attention')
plt.show()
plt.plot(eps,usg,'b',alpha=0.3)
plt.title('Cumulative Attention')
plt.xlabel('Episode')
plt.ylabel('Cumulative Attention')
plt.savefig(pp,format='pdf')

# average tests
eps,rews=np.array(test_list).T
plt.plot(eps,rews,'g',alpha=0.3)
plt.title('Average Episode Reward per 100 Episodes')
plt.xlabel('Episode')
plt.ylabel('Avg. Episode Reward')
plt.show()
plt.plot(eps,rews,'g',alpha=0.3)
plt.title('Average Episode Results per 100 Episodes')
plt.xlabel('Episode')
plt.ylabel('Avg. Episode Reward')
plt.savefig(pp,format='pdf')
pp.close()
