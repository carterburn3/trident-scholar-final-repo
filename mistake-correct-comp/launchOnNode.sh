#!/bin/bash

Xvfb $1 &
export DISPLAY=$1
echo "Display is set to $DISPLAY"
eval "python /mnt/lustre/scratch/m180732/mistake-correct-comp/quadratic.py -a 0.006 -g 0.95 -e 5000 -E 0.04 -H 12.0 -b 5000 -f $2 -p $3"