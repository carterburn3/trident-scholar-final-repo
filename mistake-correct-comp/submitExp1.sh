#!/bin/bash
# loop to submit multiple jobs of experiment 1 of mistake correcting
# H = 12.0 B = 5000

for testnum in {1..30}
do
    qsub -v DIS=:101,TESTN=$testnum,EXPN=1 /mnt/lustre/scratch/m180732/mistake-correct-comp/submitOne.sh
done

echo "Submitted all jobs"