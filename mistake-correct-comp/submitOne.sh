#!/bin/bash
# a PBS script to submit one job using the launchOnNode.sh script
# in order to launch multiple test runs of the same experiment, this script
# should be called by another script in a loop fashion
# $1 = display $2 = test number $3 = exp dir
#PBS -l walltime=20:00:00
#PBS -l select=1:mpiprocs=1:ncpus=8
#PBS -e /mnt/lustre/scratch/m180732/mistake-correct-comp/testRes/outputDumps/e.txt
#PBS -o /mnt/lustre/scratch/m180732/mistake-correct-comp/testRes/outputDumps/o.txt

source /home/users/m180732/mypy/bin/activate
aprun /mnt/lustre/scratch/m180732/mistake-correct-comp/launchOnNode.sh $DIS $TESTN $EXPN