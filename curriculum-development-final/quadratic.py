from ple import PLE
import frogger
from constants import *
import numpy as np
import pygame
from pygame.constants import K_F15,K_LEFT,K_RIGHT,K_UP,K_DOWN,K_AMPERSAND,K_HASH,K_DOLLAR,K_EXCLAIM,K_SPACE
import random
from optparse import OptionParser
from features import NonLinearFeatures
from nonlinearagent import NonLinearAgent
import os
import cPickle as pickle
from copy import deepcopy
from time import sleep, time

def parseOptions():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 1.0")
    parser.add_option("-t", "--testing",
                      action="store_true",
                      dest="testing",
                      default=False,
                      help="to enter testing mode")
    parser.add_option("-a", "--alpha",
                      action="store",
                      type="float",
                      dest="alpha",
                      default=float(0.006),
                      help="define an alpha")
    parser.add_option("-e", "--episodes",
                      action="store",
                      type="int",
                      dest="numEpisodes",
                      default=int(20000),
                      help="set number of episodes")
    parser.add_option("-g", "--gamma",
                      action="store",
                      type="float",
                      dest="discount",
                      default=float(0.9),
                      help="set discount factor")
    parser.add_option("-E", "--epsilon",
                      action="store",
                      type="float",
                      dest="epsilon",
                      default=float(0.1),
                      help="set epsilon value")
    parser.add_option("-f", "--testFileNum",
                      action="store",
                      type="int",
                      dest="saveNum",
                      default=int(0),
                      help="set test number")
    (options, args) = parser.parse_args()

    return options

def getNextState(n_s):
    if n_s['term']: return None
    else: return n_s

def runTest(agent,p):
    p.game.testingRun=True
    total=0.0
    for i in range(1):
        p.reset_game()
        s,human=p.getGameState()
        print 'TEST',i
        badStates=0
        while True:
            if p.game_over() or badStates == 100:
                break
            if s['frog_y'] > 430.0: badStates += 1
            else: badStates = 0
            action=agent.getPolicy(s)
            total+=p.act(action)
            s,human=p.getGameState()
    p.reset_game()
    p.game.testingRun=False
    return float(total/30.0)

options=parseOptions()
if (not options.saveNum) and not options.testing:
    print 'PLEASE ENTER A TEST NUMBER'
    exit(1)
    
# track the start time of the experiment
startTime = time()
game=frogger.Frogger()
fps=30
p=PLE(game,fps=fps,force_fps=False,add_noop_action=True,display_screen=True)
agent=NonLinearAgent(p.getActionSet(),None,options.testing,alpha=options.alpha,epsilon=options.epsilon,gamma=options.discount)
agent.printOut=False
reward=0.0
s=p.getGameState()
midDecay=options.numEpisodes/2
np.set_printoptions(linewidth=300)
test_list=[]

path='./testRes/test'+str(options.saveNum)+'/'

# create directory to save results in
if not os.path.exists(path):
    try: os.makedirs(path)
    except:
        print 'ERROR CREATING TEST DIRECTORY'
        exit(1)

fi=open(path+'experiment','w')
fi.write('alpha='+str(options.alpha)+' epsilon='+str(options.epsilon)+' gamma='
         +str(options.discount)+' epi='+str(options.numEpisodes))
fi.close()

def save_weights(weights,epi):
    if not os.path.exists(path+'mid-weight-save/'):
        try: os.makedirs(path+'mid-weight-save/')
        except:
            print 'ERROR CREATING WEIGHT SAVE DIRECTORY'
            exit(1)
    s=path+'mid-weight-save/epi-'+str(epi)
    fi=open(s,'w')
    pickle.dump(weights,fi)
    fi.close()

episode = 1
while episode < options.numEpisodes+1:
    print 'BEGINNING EPISODE ' + str(episode)

    reachedMidEpi=False
    printqs=False
    agent.printOut=False

    if episode >= midDecay:
        agent.epsilon = agent.epsilon * 0.5
        midDecay += (midDecay*0.5)

    if os.path.isfile('./print-weights'):
        print agent.weights
    if os.path.isfile('./print-qs'):
        printqs=True
    if os.path.isfile('./print-out'):
        agent.printOut=True

    while True:
        if p.game_over():
            break

        action=agent.pickAction(s,printqs,episode)
        reward=p.act(action)
        next_s=p.getGameState()
        n_s=getNextState(next_s)
        agent.update(s,action,n_s,reward)
        s=n_s
        if s==None: s = next_s

    p.reset_game()
    if episode%100==0:
        # run the test, first checking to make sure that test_list doesn't have this epi already
        test_list.append((int(episode), runTest(agent,p)))
    if episode%1000==0: save_weights(agent.weights,episode) # will overwrite if we changed back
            
    s=p.getGameState()
    episode += 1

fi=open(path+'test-res','w')
pickle.dump(test_list,fi)
fi.close()

'''
fi=open(path+'save-weights-'+str(options.saveNum),'w')
pickle.dump(agent.weights,fi)
fi.close()

# save the test list
fi=open(path+'save-tests-'+str(options.saveNum),'w')
pickle.dump(test_list,fi)
fi.close()
        
# save teacher usage
fi=open(path+'save-usage-'+str(options.saveNum),'w')
pickle.dump(agent.teacherUsage,fi)
fi.close()

# save all the rewards from each episode
fi=open(path+'save-rewards-'+str(options.saveNum),'w')
pickle.dump(rewards_list,fi)
fi.close()
'''
