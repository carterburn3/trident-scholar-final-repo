import pygame
import frogger
from constants import *
from ple import PLE
import numpy as np
import cPickle as pickle
from nonlinearagent import NonLinearAgent
from time import sleep

class CurriculumDevelopment:
    def __init__(self, path):
        self.path=path
        self.lastCur=None
        self.xLocs=[224.0, 192.0, 160.0, 128.0, 96.0, 64.0, 32.0, 0.0, 256.0, 288.0, 320.0, 352.0, 384.0, 416.0]
        self.yLocs=[485.0, 453.0, 421.0, 389.0, 357.0, 325.0, 293.0, 261.0, 229.0, 197.0, 165.0, 133.0, 101.0]

    def showExpOver(self):
        self.game.screen.fill((0,0,0))
        message='Experiment is over! Thank you for participating!'
        x=(kScreenWidth-self.game.backdrop.labFont.size(message)[0])/2
        label=self.game.backdrop.labFont.render(message,1,(36,253,46))
        self.game.screen.blit(label, (x,kScreenHeight/2))
        pygame.display.update()
        sleep(5)

    def init(self):
        self.game = frogger.Frogger()
        self.p=PLE(self.game,force_fps=False,add_noop_action=True,display_screen=True)
        self.game.step(0)
        self.game.clock=None

        self.settingFrog=False
        self.selectedRow=None
        self.training=False

    def getNextState(self, n_s):
        if n_s['term']: return None
        else: return n_s

    def convertToFrogPos(self, pos):
        screenPos=(pos[0]-kXOffset,float(pos[1]))
        # click is where we want to place the center of the Frog, so we adjust for
        # top of the rect
        newX,newY = screenPos[0]-12.0, screenPos[1]-12.0
        # now we want to 'snap' to the closest x and y position
        # if we are greater than kPlayYRiverLimit (in road/median), we will snap x AND y
        # if we are less, (else) we will only snap y, x is up to the user actually
        # so only snap x if y > kPlayYRiver, always snap y
        finalX,finalY=newX,newY
        if newY > kPlayYRiverLimit:
            # snap x
            finalX=min(self.xLocs, key=lambda x:abs(x-newX))
        # always snapping y (only snapping if not in a home)
        if newY > 90.0:
            finalY=min(self.yLocs, key=lambda x:abs(x-newY))

        #return (newX,newY)
        return (finalX,finalY)

    def getSelectedRow(self, pos):
        # get row to move and update (row based on the click position)
        new=None
        if pos[1] >= kPlayYCar[0]+self.game.cars[0].get_rect().height: new=None
        elif pos[1] > kPlayYCar[0]: new=0
        elif pos[1] > kPlayYCar[1] and pos[1] <= kPlayYCar[0]: new=1
        elif pos[1] > kPlayYCar[2] and pos[1] <= kPlayYCar[1]: new=2
        elif pos[1] > kPlayYCar[3] and pos[1] <= kPlayYCar[2]: new=3
        elif pos[1] > kPlayYCar[4] and pos[1] <= kPlayYCar[3]: new=4
        elif pos[1] > 261.0 and pos[1] >= kPlayYRiver[0]+self.game.river_group.sprites()[0].get_rect().height: new=None
        elif pos[1] > kPlayYRiver[0] and pos[1] <= 261.0: new=5
        elif pos[1] > kPlayYRiver[1] and pos[1] <= kPlayYRiver[0]: new=6
        elif pos[1] > kPlayYRiver[2] and pos[1] <= kPlayYRiver[1]: new=7
        elif pos[1] > kPlayYRiver[3] and pos[1] <= kPlayYRiver[2]: new=8
        elif pos[1] > kPlayYRiver[4]: new=9
        else: new=None

        if new==None or new==self.selectedRow: return None
        return new

    def saveState(self):
        locs=[None]*29
        for i in range(27):
            locs[i] = self.game.getLocOfSpriteAtLabel(i)

        locs[27] = (self.game.frog.get_pos(), self.game.frog.direction)
        homeInfo=[False]*5
        for i in range(len(self.game.homes)):
            if self.game.homes[i].frogged: homeInfo[i] = True
        locs[28] = homeInfo
        return locs

    def restoreState(self):
        # update rivers
        for i in range(16):
            self.game.riverMap[i].set_pos(self.lastCur[i])
        # cars
        for i in range(16,27):
            self.game.carMap[i].set_pos(self.lastCur[i])

        # frog
        self.game.frog.set_pos(self.lastCur[27][0])
        self.game.frog.direction=self.lastCur[27][1]

        # set up the homes
        for i in range(len(self.game.homes)):
            if self.lastCur[28][i]:
                self.game.homes[i].homeFrog()
                self.game.numFrogsHomed+=1
                continue
        # draw the update with no row selected :)
        self.game.drawCurDevUpdates(None)

    def oneDevelopment(self):
        if self.lastCur != None:
            # restore the state here
            self.restoreState()
        self.game.drawCurDevUpdates(None)
        while not self.training:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit(0)

                if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                    if self.settingFrog:
                        self.game.frog.set_pos(self.convertToFrogPos(event.pos))
                        self.game.drawCurDevUpdates(self.selectedRow)
                        pygame.display.update()
                    else:
                        self.selectedRow=self.getSelectedRow(event.pos)
                        self.game.backdrop.drawSelectedRowMessage(self.selectedRow, self.game.screen)

                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 4:
                        self.game.moveRowRight(self.selectedRow)
                    if event.button == 5:
                        self.game.moveRowLeft(self.selectedRow)

                elif event.type == pygame.constants.KEYDOWN:
                    if event.key == pygame.constants.K_r:
                        # toggle placing the frog
                        self.settingFrog = not self.settingFrog
                        self.selectedRow = None
                        if self.settingFrog:
                            # if setting the frog now, get rid of row message
                            self.game.backdrop.postedLab=self.game.backdrop.frogSetLabel
                            self.game.backdrop.drawSelectedRowMessage(self.selectedRow, self.game.screen)
                        else:
                            self.game.backdrop.postedLab=self.game.backdrop.rowSetLabel
                            self.game.backdrop.drawSelectedRowMessage(self.selectedRow, self.game.screen)
                    if event.key == pygame.constants.K_LEFT and self.selectedRow != None:
                        self.game.moveRowLeft(self.selectedRow)
                    if event.key == pygame.constants.K_RIGHT and self.selectedRow != None:
                        self.game.moveRowRight(self.selectedRow)
                    if event.key == pygame.constants.K_UP and self.selectedRow != None:
                        self.game.backdrop.movementNumber += 1
                        if self.game.backdrop.movementNumber > 10: self.game.backdrop.movementNumber = 10
                        self.game.backdrop.drawSelectedRowMessage(self.selectedRow, self.game.screen)
                    if event.key == pygame.constants.K_DOWN and self.selectedRow != None:
                        self.game.backdrop.movementNumber -= 1
                        if self.game.backdrop.movementNumber < 1: self.game.backdrop.movementNumber = 1
                        self.game.backdrop.drawSelectedRowMessage(self.selectedRow, self.game.screen)
                    if event.key == pygame.constants.K_SPACE:
                        # done setting up the scene
                        self.training = True
                        self.game.clock = pygame.time.Clock()
                        self.game.step(0)
                        break

                    if event.key == pygame.constants.K_q:
                        # save the last saved weights and exit
                        self.showExpOver()
                        fi=open(self.path+'finalWeights','w')
                        if not os.path.exists(path+'curDevWeights'):
                            fi.close()
                            pygame.quit()
                            exit(0)
                        fi2=open(self.path+'curDevWeights')
                        pickle.dump(pickle.load(fi2),fi)
                        fi.close()
                        fi2.close()
                        pygame.quit()
                        exit(0)
                else:
                    break

                if self.settingFrog and (not self.game.checkFrogAlive() or not self.game.checkOnScreen()):
                    print 'Resetting Frog position'
                    self.game.frog.set_pos(kPlayFrog)
                    self.game.backdrop.postedLab=self.game.backdrop.frogSetLabel
                    self.game.drawCurDevUpdates(self.selectedRow)
                    pygame.display.update()
                    continue
                else:
                    if self.settingFrog: self.game.backdrop.postedLab=self.game.backdrop.frogSetLabel
                    break

        self.game.backdrop.postedLab=self.game.backdrop.blankLabel
        # save the current state right here!
        self.lastCur=self.saveState()
        self.trainingPhase()
        return

    def trainingPhase(self):
        agent = NonLinearAgent(self.p.getActionSet(),None,False)
        agent.weights=agent.features.loadCurWeights(self.path)
        printqs=False

        reward = 0.0
        s=self.p.getGameState()
        episode=1

        while True:
            if self.p.game_over():
                break

            action=agent.pickAction(s,printqs,episode)
            reward=self.p.act(action)
            next_s=self.p.getGameState()
            n_s=self.getNextState(next_s)
            agent.update(s,action,n_s,reward)
            s=n_s
            if s==None: s=next_s

        fi=open(self.path+'curDevWeights','w')
        pickle.dump(agent.weights,fi)
        fi.close()
        return
