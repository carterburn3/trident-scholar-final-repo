from pygame import Rect,sprite
from copy import deepcopy
from constants import *
import numpy as np

class NonLinearFeatures():
    def __init__(self):
        self.numCols=int(3)
        self.numRows=int(3)

    def getFN(self, i, j):
        num=3*j+i
        if num >= 4: num-=1
        return num

    def getFeatures(self, state):
        features=dict()
        fRect=Rect((state['frog_x'],state['frog_y']),(32.0,32.0))
        sX=float(state['frog_x']-32.0)
        sY=float(state['frog_y']-32.0) # start x and y for the 32x32 pixel boxes
        # set frog_y value as 4th feature
        features[16]=float(int(state['frog_y']/32)/15.0) # not sure if this should be /32 or not (definitely want int) # normalized to be in range [0,1]
        # finding sprites in our surrounding boxes
        boxes=[None]*self.numCols
        for i in range(self.numCols):
            boxes[i]=[None]*self.numRows

        # create 32x32 pixel boxes around frog
        for i in range(self.numCols):
            for j in range(self.numRows):
                if (i != 1 or j != 1):
                    boxes[i][j]=Rect((sX+(32.0*i),sY+(32.0*j)),(32.0,32.0))


        for i in range(self.numCols):
            for j in range(self.numRows):
                if boxes[i][j]==None: continue
                if boxes[i][j].collidelist(state['cars']) != -1:
                    features[self.getFN(i,j)]=float(1)
                    features[self.getFN(i,j)+8]=float(0)
                else:
                    features[self.getFN(i,j)]=float(0)
                    features[self.getFN(i,j)+8]=float(0)

        # river collision calculation
        newFrog=deepcopy(state['frog'])
        for i in range(self.numCols):
            for j in range(self.numRows):
                if boxes[i][j]==None: continue
                newFrog.set_pos((sX+(32.0*i),sY+(32.0*j)))
                h=sprite.spritecollide(newFrog,state['riverS'],False)
                if len(h)==0:
                    # no collision in this box, keep as is
                    pass
                elif (((3*j)+i) == 3) or (((3*j)+i)==5):
                    if newFrog.attachedObj != None and h[0].get_pos() == newFrog.attachedObj.get_pos():
                        # pos of the collision and the frog's attached object is the same, this is okay
                        features[self.getFN(i,j)+8]=float(1)
                else:
                    # feature fired in front or behind, that's fine set feature for this box
                    features[self.getFN(i,j)+8]=float(1)

        # home collision
        if state['frog_y'] <= kPlayYRiver[4]:
            if boxes[0][0].collidelist(state['homeR']) != -1:
                features[8]=float(1)
            else: features[8]=float(0)
            if boxes[1][0].collidelist(state['homeR']) != -1:
                features[9]=float(1)
            else: features[9]=float(0)
            if boxes[2][0].collidelist(state['homeR']) != -1:
                features[10]=float(1)
            else: features[10]=float(0)

        # side collision in the river
        if state['frog_y'] <= kPlayYRiverLimit and state['frog_x'] <= 5.0:
            features[0]=float(1)
            features[3]=float(1)
            features[5]=float(1)
        if state['frog_y'] <= kPlayYRiverLimit and state['frog_x'] >= 430.0:
            features[2]=float(1)
            features[4]=float(1)
            features[7]=float(1)

        return features
    
    def initializeWeights(self):
        weightsBin=np.zeros((16,16))
        weightsSqr=np.zeros((17,1))
        weightsLin=np.zeros((1,17))

        return weightsBin,weightsSqr,weightsLin

    def loadWeights(self):
        # load pickle dump
        weights=None
        # find max number in directory
        import cPickle as pickle
        import glob,os
        f=os.getcwd()+'/save-weights-*'
        nums=[int(x[-1]) for x in glob.glob(f)]
        if len(nums) == 0:
            # can't test without weights file, exit
            print 'No weights file found...\nExiting...'
            exit(1)
        num=max(nums)
        fi=open('save-weights-'+str(num))
        print 'opening save-weights-'+str(num)+'...'
        weights=pickle.load(fi)
        fi.close()

        return weights

    def loadCurWeights(self, path):
        import cPickle as pickle
        import os
        weights=None
        if os.path.exists(path+'curDevWeights'):
            fi=open(path+'curDevWeights')
            #print 'opening',path+'curDevWeights'
            weights=pickle.load(fi)
            fi.close()
        else:
            # need to just initiliaze 
            weights=[self.initializeWeights() for x in range(5)]
        return weights
