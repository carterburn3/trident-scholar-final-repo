import pygame
from pygame.constants import K_EXCLAIM,K_AMPERSAND,K_DOLLAR,K_HASH,K_F15,K_SPACE,K_UP,K_DOWN,K_LEFT,K_RIGHT
import ple
from ple.games import base
from constants import *
import os, sys, time
from sys import stderr as err
from frog_sprites import *
from supporter import *

class Backdrop:
    def __init__(self, SCREEN_WIDTH, SCREEN_HEIGHT, image_background):
        self.SCREEN_WIDTH = SCREEN_WIDTH
        self.SCREEN_HEIGHT = SCREEN_HEIGHT

        self.background_image = image_background
        self.x = 0
        self.y = 0
        self.leftOffset = None
        self.rightOffset = None
        self.rect = self.background_image.get_rect()
        self.rect.left = self.x + kXOffset
        self.rect.top = self.y

        self.font=pygame.font.SysFont("monospace", 30)
        self.rowLabelLeft =self.font.render(">", 1, (36,253,46))
        self.rowLabelRight=self.font.render("<", 1, (36,253,46))
        self.movementNumber=int(5)
        self.numberLabel  =self.font.render(str(self.movementNumber), 1, (36,253,46))
        self.labFont=pygame.font.SysFont("monospace", 20)
        self.rowSetLabel=self.labFont.render('Setting rows',1,(36,253,46))
        self.frogSetLabel=self.labFont.render('Setting frog',1,(36,253,46))
        self.blankLabel=self.labFont.render('',1,(36,253,46))
        self.postedLab=self.rowSetLabel
        self.leftRows=[0,2,4,5,8]
        self.leftLabel=self.font.render('<<',1,(36,253,46))
        self.rightLabel=self.font.render('>>',1,(36,253,46))

    def draw_background(self, screen):
        screen.fill((0,0,0))
        screen.blit(self.background_image, (self.rect.left,self.rect.top))

    def draw_outerEdge(self, screen):
        if self.leftOffset == None and self.rightOffset == None:
            self.leftOffset = screen.subsurface(pygame.Rect(0,0,kXOffset,self.SCREEN_HEIGHT))
            self.rightOffset = screen.subsurface(pygame.Rect(kXOffset+kPlayWidth,0,self.SCREEN_WIDTH-(kXOffset+kPlayWidth),self.SCREEN_HEIGHT))
        self.leftOffset.fill((0,0,0))
        self.rightOffset.fill((0,0,0))
        screen.blit(self.postedLab, (0,0))
        if self.postedLab!=self.blankLabel: self.drawArrows()

    def getMiddle(self, row):
        # det y loc
        if row<5: locs=kPlayYCar
        else:
            locs=kPlayYRiver
            row-=5
        upper=locs[row]
        lower=None
        if row:
            # can subtract
            lower=locs[row-1]
        else:
            if locs[0] == 418.0: lower=453.0
            else: lower=261.0
        # have upper and lower, need the middle
        return ((lower-upper)/2)+upper

    def drawSelectedRowMessage(self, row, screen):
        self.draw_outerEdge(screen)
        if row != None:
            locs=None
            '''# det y loc
            if row<5: locs=kPlayYCar
            else:
                locs=kPlayYRiver
                row-=5
            upper=locs[row]
            lower=None
            if row:
                # can subtract
                lower=locs[row-1]
            else:
                if locs[0] == 418.0: lower=453.0
                else: lower=261.0
            # have upper and lower, need the middle
            middle=((lower-upper)/2)+upper'''
            middle=self.getMiddle(row)
            y=middle-(self.font.size('<')[1]/2)
            xLeft=kXOffset-self.font.size('<')[0]
            xRight=0
            
            #self.leftOffset.blit(self.rowLabelLeft, (xLeft,y))
            # NO MORE LEFT SIDE ARROW, JUST THE RIGHT SIDE!
            self.rightOffset.blit(self.rowLabelRight, (xRight,y))
            # re-render the number label
            self.numberLabel=self.font.render(str(self.movementNumber),1,(36,253,46))
            self.rightOffset.blit(self.numberLabel, (self.font.size('<')[0]*2, y))

        pygame.display.update()    

    def drawArrows(self):
        for i in range(10):
            middle=self.getMiddle(i)
            y=middle-(self.font.size('<')[1]/2)
            # blit at this point on y
            x=kXOffset-(2*self.font.size('<')[0])
            # determine the direction
            label=None
            if i in self.leftRows: label=self.leftLabel
            else: label=self.rightLabel
            
            self.leftOffset.blit(label, (x,y))
            

class Frogger(base.PyGameWrapper):
    def __init__(self, width=kScreenWidth, height=kScreenHeight):
        actions = {
            "up": K_EXCLAIM,
            "right": K_HASH,
            "down": K_DOLLAR,
            "left": K_AMPERSAND
        }

        fps = 30
        base.PyGameWrapper.__init__(self, width, height, actions=actions)

        self.images={}
        pygame.display.set_mode((1,1),pygame.NOFRAME)

        self.support = Supporter(self)
        
        self._dir_ = os.path.dirname(os.path.abspath(__file__))
        self._data_dir = os.path.join(self._dir_, "data/")
        self.support._load_images()

        self.support.set_rewards()

        self.backdrop = None
        self.frog = None

    def init(self):
        self.backdrop = Backdrop(self.width,self.height,self.images["background"])

        # frog init
        self.frog = Frog(kPlayFrog, self.images["frog"]["stationary"])

        # place homes
        self.support.init_homes()
        self.numFrogsHomed=0

        self.support.init_cars()
        self.support.init_river()

        self.mapCars()
        self.mapRivers()

        self.bonusCroc=BonusRandom(kCrocodileBonusRate,kCrocodileBonusDelay)
        self.bonusFly=BonusRandom(kFlyBonusRate,kFlyBonusDelay)

        self.reachedMidway = False

        self.score = 0.0
        self.lives = 1
        self.game_tick = 0

        self.term=False

    def mapCars(self):
        self.carMap={}
        for car in self.cars:
            self.carMap[car.getLabel()] = car

    def mapRivers(self):
        self.riverMap={}
        for river in self.river_group.sprites():
            self.riverMap[river.getLabel()] = river

    def getLocOfSpriteAtLabel(self, label):
        if label < 16: return self.riverMap[label].get_pos()
        else: return self.carMap[label].get_pos()

    def getScore(self):
        return self.score

    def getGameState(self):
        homeStatus=[None]*len(self.homes)
        for i in range(len(self.homes)):
            if self.homes[i].frogged == True: homeStatus[i] = float(0.66) # fly is worth more
            elif self.homes[i].flied == True: homeStatus[i] = float(1)
            elif self.homes[i].croced == True: homeStatus[i] = float(0.33) # let's frog know what's here
            else: homeStatus[i] = 0

        state = {
            'frog_x': self.frog.get_pos()[0],
            'frog_y': self.frog.get_pos()[1],
            'rect_w': self.frog.get_rect().width,
            'rect_h': self.frog.get_rect().height,
            'cars': self.support.carRects(),
            #'rivers': self.support.riverRects(),
            'riverS': self.river_group.sprites(),
            'homes': homeStatus,
            'homeR': self.support.homeRects(),
            'frog': self.frog,
            'term': self.term,
            'carInf': self.cars,
            'riverInf': self.river_group.sprites(),
            'homeInf': self.homes
        }

        ''' for testing:
        ,
        'frog':self.frog,
        'riverS':self.river_group.sprites()'''

        if self.term: self.term=False

        #return state
        return state
        '''if self.nextActionFromHuman:
            self.nextActionFromHuman = False
            return state,True
        else:
            return state,False'''

    def _handle_player_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                key = event.key
                if key == self.actions["up"]:
                    self.frog.set_move((0.0,-1.0))
                if key == self.actions["right"]:
                    self.frog.set_move((1.0,0.0))
                if key == self.actions["down"]:
                    self.frog.set_move((0.0,1.0))
                if key == self.actions["left"]:
                    self.frog.set_move((-1.0,0.0))

    def game_over(self):
        if self.numFrogsHomed == 5:
            # game over because we won
            return True
        return self.lives <= 0
            
    def drawUpdates(self):
        self.backdrop.draw_background(self.screen)
        for home in self.homes: home.draw(self.screen)
        for car in self.cars: car.draw(self.screen)
        self.river_group.draw(self.screen)
        self.frog.draw(self.screen)
        self.backdrop.draw_outerEdge(self.screen)
        pygame.display.update()

    def drawCurDevUpdates(self, row):
        self.backdrop.draw_background(self.screen)
        for home in self.homes: home.draw(self.screen)
        for car in self.cars: car.draw(self.screen)
        self.river_group.draw(self.screen)
        self.frog.draw(self.screen)
        # first draw the row directions
        self.backdrop.drawSelectedRowMessage(row, self.screen)
        pygame.display.update()
   
    def moveRowLeft(self, row):
        # doing nothing if no row selected
        if row != None:
            # need to move left, which is negative on the screen (may be positive dt!)
            yCoord=None
            sprites=None
            moveVal=self.backdrop.movementNumber*99
            if row<5:
                yCoord=kPlayYCar[row]
                sprites=self.cars
            else:
                yCoord=kPlayYRiver[row-5]
                sprites=self.river_group.sprites()

            for s in sprites:
                # if yCoord equals this sprites y-loc, move it!
                if s.get_pos()[1] == yCoord:
                    # if speed is positive and going left, make dt negative
                    if s.get_speed()[0] > 0: s.curDevUpdate(-1*moveVal)
                    else: s.curDevUpdate(moveVal)
            # draw the updates
            self.drawCurDevUpdates(row)

    def moveRowRight(self, row):
        # doing nothing if no row selected
        if row != None:
            # need to move right, which is a positive speed[0]*dt
            yCoord=None
            sprites=None
            moveVal=self.backdrop.movementNumber*99
            if row<5:
                yCoord=kPlayYCar[row]
                sprites=self.cars
            else:
                yCoord=kPlayYRiver[row-5]
                sprites=self.river_group.sprites()

            for s in sprites:
                # if yCoord equals this sprite's y-loc, move it
                if s.get_pos()[1] == yCoord:
                    if s.get_speed()[0] < 0: s.curDevUpdate(-1*moveVal)
                    else: s.curDevUpdate(moveVal)

            # draw the updates
            self.drawCurDevUpdates(row)

    def checkFrogAlive(self):
        if self.frog._y < kPlayYHomeLimit:
            collideInd = self.frog.get_rect().collidelist(self.homeRects)
            if collideInd != -1 and self.homes[collideInd].frogged == False and self.homes[collideInd].croced == False:
                # in home, place frog in home and spawn another one at the bottom (return false at
                # end)
                self.homes[collideInd].homeFrog()
                self.numFrogsHomed+=1
                return False
            elif self.homes[collideInd].frogged:
                self.homes[collideInd].frogged=False
                self.homes[collideInd].flied=False
                self.homes[collideInd].croced=False
                self.homes[collideInd].image = self.homes[collideInd].image_assets["blank"]
            else:
                return False
        if self.frog._y < kPlayYRiverLimit:
            h=pygame.sprite.spritecollide(self.frog, self.river_group, False)
            if len(h) == 0:
                return False # fell in the river
            else:
                # first attach the Frog to the object
                self.frog.attachTo(h[0])
                # check if offscreen
                if self.frog.attachDisappeared():
                    return False
        else:
            # in the road, check for collisions with cars
            collideInd=self.frog.get_rect().collidelist(self.support.carRects())
            if collideInd != -1:
                return False # colliding with a car
        return True # alive!

    def checkOnScreen(self):
        if (self.frog._x+(self.frog.rect.width/2)) < 0.0 or (self.frog._x+(self.frog.rect.width/2)) > kPlayWidth:
            return False
        if (self.frog._y+(self.frog.rect.height/2)) < 0.0 or (self.frog._y+(self.frog.rect.height/2)) > kPlayHeight:
            return False
        return True

    def step(self, dt):
        self.game_tick += 1
        #dt = dt / 1000.0

        self._handle_player_events()
        #if self.humanWaiting and dt > 0: return

        # all sprites do their update
        for home in self.homes: home.update(dt)
        for car in self.cars: car.update(dt)
        self.river_group.update(dt)
        self.frog.update(dt)

        # check for collisions with the homes first
        if self.frog._y < kPlayYHomeLimit:
            collideInd = self.frog.get_rect().collidelist(self.homeRects)
            if collideInd != -1 and self.homes[collideInd].frogged == False and self.homes[collideInd].croced == False:
                # hit an open home, kill frog and init a new one
                if self.homes[collideInd].flied == True:
                    # fly bonus --> give reward here
                    print 'fly bonus!'
                self.homes[collideInd].homeFrog()
                self.score += self.rewards['home']
                self.numFrogsHomed += 1
                if self.numFrogsHomed == 5: self.score += self.rewards['win']
                self.frog.kill()
                self.term=True
                self.frog = Frog(kPlayFrog, self.images["frog"]["stationary"])
                self.reachedMidway = False
            else:
                self.lives -= 1
                self.score += self.rewards['death']
                self.term=True
        elif self.frog._y < kPlayYRiverLimit:
            # in the river zone
            h=pygame.sprite.spritecollide(self.frog,self.river_group,False)
            if len(h) == 0:
                # fell in the river
                self.lives -= 1
                self.score += self.rewards['death']
                self.term=True
            else:
                # if haven't been given the midway reward, give it now
                if self.reachedMidway == False:
                    self.score += self.rewards['midway']
                    self.reachedMidway = True
                # attach to h[0] (first check that we aren't hopping laterally)
                if self.frog.attached and h[0] != self.frog.attachedObj:
                    if h[0].get_rect()[1] == self.frog.attachedObj.get_rect()[1]:
                    # tried to move laterally between attached objects, can't do that, kill
                        self.lives -= 1
                        self.score += self.rewards['death']
                        self.term=True
                    else:
                        self.frog.attachTo(h[0])
                        if self.frog._y <= kPlayYRiver[0] and self.frog._y >= kPlayYRiver[1]:
                            self.score += float(0.1)
                        elif self.frog._y <= kPlayYRiver[1] and self.frog._y >= kPlayYRiver[2]:
                            self.score += float(0.2)
                        elif self.frog._y <= kPlayYRiver[3] and self.frog._y >= kPlayYRiver[4]:
                            self.score += float(0.4)
                        elif self.frog._y <= kPlayYRiver[2] and self.frog._y >= kPlayYRiver[3]:
                            self.score += float(0.3)
                        elif self.frog._y <= kPlayYRiver[4]:
                            self.score += float(0.5)
                else:
                    self.frog.attachTo(h[0])

                # check if frog has gone off screen while attached to object, kill
                if self.frog.attachDisappeared():
                    self.lives -= 1
                    self.score += self.rewards['death']
                    self.term=True

                # check for diving turtle
                if isinstance(self.frog.attachedObj, Turtle) and self.frog.attachedObj.disappeared == True:
                    # if frog is attached to a disappeared Turtle, kill
                    '''self.lives -= 1
                    self.score += self.rewards['death']
                    self.term=True'''
                    pass
        else:
            # in the road zone, check for collisions with cars
            # if we have been given the midway reward and went back to the road, need to penalize
            if self.reachedMidway == True:
                self.score += self.rewards['downmid']
                #self.reachedMidway = False
            collideInd = self.frog.get_rect().collidelist(self.support.carRects())
            '''if self.frog.attached:
                self.frog.attached = False
                self.frog.attachedObj = None'''
            if collideInd != -1:
                # collided with a car, kill the frog
                self.lives -= 1
                self.score += self.rewards['death']
                self.term=True

            # may need to add the car reward
            '''
            if self.frog._y <= kPlayYCar[0] and self.frog._y >= kPlayYCar[1]:
                self.score += float(0.1)
            elif self.frog._y <= kPlayYCar[1] and self.frog._y >= kPlayYCar[2]:
                self.score += float(0.2)
            elif self.frog._y <= kPlayYCar[2] and self.frog._y >= kPlayYCar[3]:
                self.score += float(0.3)
            elif self.frog._y <= kPlayYCar[3] and self.frog._y >= kPlayYCar[4]:
                self.score += float(0.4)
            elif self.frog._y <= kPlayYCar[4]:
                self.score += float(0.5)
            '''

        
        # determine if homes should get a croc/fly placed
        if self.bonusCroc.get_chance(self.clock.get_time()): self.homes[int(5*random.random())].setCroc()
        if self.bonusFly.get_chance(self.clock.get_time()): self.homes[int(5*random.random())].setFly()

        self.drawUpdates()
