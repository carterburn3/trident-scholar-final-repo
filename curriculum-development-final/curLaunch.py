import pygame
import frogger
from constants import *
from ple import PLE
import numpy as np
import cPickle as pickle
from nonlinearagent import NonLinearAgent
from curDev import CurriculumDevelopment
from time import time
from optparse import OptionParser
import os

def parseOptions():
    parser=OptionParser(usage='usage: %prog [options]', version='%prog 1.0')
    parser.add_option('-N', '--number',
                      action='store',
                      type='str',
                      dest='number',
                      default='000000',
                      help='define alpha number')
    (options,args)=parser.parse_args()
    return options

options=parseOptions()
np.set_printoptions(linewidth=300)
# first we are going to do curriculum dev, then we will train regularly with those developed weights
# user will get a time limit of 10 minutes (600 seconds)
startTime=time()
path='/home/mids/m180732/experiment-results/cur-dev-res/'+str(options.number.replace(" ",""))+'/'
# make a quick dump file
if not os.path.exists(path):
    try: os.makedirs(path)
    except:
        print 'ERROR CREATING TEST DIRECTORY'
        exit(1)

curDev=CurriculumDevelopment(path)
pygame.init()

while True:
    if int(time() - startTime) > 600:
        print 'YOU RAN OUT OF TIME!'
        curDev.showExpOver()
        break

    # do one development for each loop
    curDev.init()
    curDev.oneDevelopment()

# now we're on to regular training, since we aren't going to do this training when the user
# is actually using it, we are going to just save the weights as a final version and train later
fi=open(path+'finalWeights','w')
if not os.path.exists(path+'curDevWeights'):
    fi.close()
    exit()
fi2=open(path+'curDevWeights')
pickle.dump(pickle.load(fi2),fi)
fi.close()
fi2.close()
    
