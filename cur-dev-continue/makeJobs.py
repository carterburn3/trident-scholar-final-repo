import os
from time import sleep

def getCurJobs():
    x=os.system("qstat | grep m180732 | wc -l > .statFile")
    sleep(1)
    fi=open('.statFile')
    l=int(fi.read())
    fi.close()
    return l

def submitJob(alpha, tests):
    s=''
    for item in tests:
        s=s+str(item)+'-'
    s=s[:-1]

    os.environ['A1']=alpha
    os.environ['J1']=s
    os.system('qsub -v ALPHA1=$A1,JOBS1=$J1 /mnt/lustre/scratch/m180732/cur-dev-continue/job-1apr.sh')
    print 'submitted',alpha,s

tests={}
#for alpha in os.listdir('/home/burn/human-systems/experiment-results/cur-dev-res/'):
'''
for alpha in os.listdir('/mnt/lustre/scratch/m180732/experiment-results/cur-dev-res/'):
    tests[alpha] = [x for x in range(1,31)]
'''
#tests={'196360':[17,18,19,22,25,26],'214536':[29],'217086':[1],'217302':[22,28,29,30]}
#tests={'215922':[29],'211974':[21],'211458':[16,17,18,20],'196360':[14]}
#tests={'217302':[27],'217086':[9,30,23,17,11],'215922':[23,12],'211998':[4,24,14,10],'211974':[4,27,21],'211458':[30,28,1],'210480':[27,18],'210030':[29,15],'196360':[7,8,5,30,3,29,28,27,26,25,24,21,20,2,18,17,14,12,11,1],
#       '185412':[4],'180780':[14,12]}

tests={'196360':[1,11,12,14,17,18,20,21,24,25,28,29,3,5,7,8]}

jobList={}
for alpha in tests:
    if len(tests[alpha]) > 15:
        split=len(tests[alpha])/2
        jobList[alpha]=[tests[alpha][:split], tests[alpha][split:]]
    else:
        jobList[alpha]=[tests[alpha][:]]

alphas=jobList.keys()

# for now we are just going to call the quadratic file! uncomment below for grace
for i in range(len(alphas)):
    curJobs=getCurJobs()
    limit=None
    if len(jobList[alphas[i]]) == 1: limit=25
    else: limit=24
    while curJobs >= limit:
        sleep(600)
        curJobs=getCurJobs()

    # no more waiting, submit
    submitJob(alphas[i], jobList[alphas[i]][0])
    if len(jobList[alphas[i]]) == 2: submitJob(alphas[i], jobList[alphas[i]][1])

print 'all done, check failures later'
