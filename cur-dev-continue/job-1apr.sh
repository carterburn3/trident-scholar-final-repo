#!/bin/bash
#PBS -l walltime=20:00:00
#PBS -l select=1:mpiprocs=24:ncpus=24

source /home/users/m180732/mypy/bin/activate
aprun -n 1 -d 24 /mnt/lustre/scratch/m180732/cur-dev-continue/nodeLaunch.sh $ALPHA1 $JOBS1
