from ple import PLE
import frogger
from constants import *
import numpy as np
import pygame
from pygame.constants import K_F15,K_LEFT,K_RIGHT,K_UP,K_DOWN,K_a,K_d,K_s,K_w
import random
from optparse import OptionParser
from features import NonLinearFeatures
from nonlinearagent import NonLinearAgent
import os
import cPickle as pickle

def parseOptions():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 1.0")
    parser.add_option("-t", "--testing",
                      action="store_true",
                      dest="testing",
                      default=False,
                      help="to enter testing mode")
    parser.add_option("-a", "--alpha",
                      action="store",
                      type="float",
                      dest="alpha",
                      default=float(0.006),
                      help="define an alpha")
    parser.add_option("-e", "--episodes",
                      action="store",
                      type="int",
                      dest="numEpisodes",
                      default=int(20000),
                      help="set number of episodes")
    parser.add_option("-g", "--gamma",
                      action="store",
                      type="float",
                      dest="discount",
                      default=float(0.9),
                      help="set discount factor")
    parser.add_option("-E", "--epsilon",
                      action="store",
                      type="float",
                      dest="epsilon",
                      default=float(0.1),
                      help="set epsilon value")
    parser.add_option('-A', '--alphaNum',
                      action='store',
                      type='str',
                      dest='alphaNum',
                      default='000000',
                      help='define alpha code for testing')
    parser.add_option('-N', '--testNum',
                      action='store',
                      type='int',
                      dest='testNum',
                      default=0,
                      help='define test number')
    (options,args) = parser.parse_args()
    return options

def getNextState(n_s):
    if n_s['term']: return None
    else: return n_s

def runTest(agent,p):
    total=0.0
    # 30 test runs
    for i in range(30):
        p.reset_game()
        s=p.getGameState()
        print 'TEST',i
        badStates=0
        while True:
            if p.game_over() or badStates == 100:
                break
            if s['frog_y'] > 430.0:
                badStates += 1
            else: badStates = 0
            action=agent.getPolicy(s)
            total+=p.act(action)
            s=p.getGameState()
    p.reset_game()
    return float(total / 30.0)

options=parseOptions()
if options.alphaNum == '000000':
    print 'PLEASE ENTER ALPHA TO CONTINUE'
    exit(1)
if not options.testNum:
    print 'PLEASE ENTER A TEST NUM'
    exit(1)
# first load up the person's information
# CHANGE PATH WHEN ON GRACE!
#humanPath='/home/burn/human-systems/experiment-results/cur-dev-res/'+str(options.alphaNum)+'/'
humanPath='/mnt/lustre/scratch/m180732/experiment-results/cur-dev-res/'+str(options.alphaNum)+'/'
if not os.path.exists(humanPath):
    print 'Results for this alpha do not exist! Please check alpha'
    exit(1)
fi=open(humanPath+'rewards')
rewards=pickle.load(fi)
fi.close()

# first need to get the episode for this alpha
# we start at the len of rewards
episode=len(rewards)
print episode
fi=open(humanPath+'finalWeights')
startWeights=pickle.load(fi)
fi.close()

# set path to save the continuation results
#path='/home/burn/human-systems/cur-dev-continue/testRes/'+str(options.alphaNum)+'/test'+str(options.testNum)+'/'
path='/mnt/lustre/scratch/m180732/cur-dev-continue/testRes/'+str(options.alphaNum)+'/test'+str(options.testNum)+'/'
if not os.path.exists(path):
    try: os.makedirs(path)
    except:
        print 'ERROR CREATING TEST DIRECTORY'
        exit(1)

fi=open(path+'continueExp','w')
fi.write('Continuing Human Training of '+str(options.alphaNum)+'\nEpisode: ' + str(episode) +'\n')
fi.close()

game=frogger.Frogger()
fps=30
p=PLE(game,fps=fps,force_fps=False,add_noop_action=True,display_screen=True)
agent=NonLinearAgent(p.getActionSet(),None,options.testing,alpha=options.alpha,epsilon=options.epsilon,gamma=options.discount,weights=startWeights)
agent.printOut=False
reward=0.0
s=p.getGameState()
midDecay=options.numEpisodes/2
np.set_printoptions(linewidth=300)
test_list=[]
printqs=False

def save_weights(weights,epi):
    if not os.path.exists(path+'mid-weight-save/'):
        try: os.makedirs(path+'mid-weight-save/')
        except:
            print 'ERROR CREATING MID WEIGHT SAVE DIRECTORY'
            exit(1)
        s=path+'mid-weight-save/epi-'+str(epi)
        fi=open(s,'w')
        pickle.dump(weights,fi)
        fi.close()

while episode < options.numEpisodes+1:
    print 'BEGINNING EPISODE ' + str(episode)

    printqs=False
    agent.printOut=False

    if episode >= midDecay:
        agent.epsilon = agent.epsilon * 0.5
        midDecay += (midDecay*0.5)

    if os.path.isfile('./print-weights'): print agent.weights
    if os.path.isfile('./print-qs'): printqs=True
    if os.path.isfile('./print-out'): agent.printOut=True

    while True:
        if p.game_over():
            break

        action=agent.pickAction(s,printqs)
        reward=p.act(action)
        next_s=p.getGameState()
        n_s=getNextState(next_s)
        agent.update(s,action,n_s,reward)
        s=n_s
        if s==None: s=next_s

    p.reset_game()
    if episode%100==0:
        # run 30 tests, this just appends to the test list, we'll get the other ones from the
        # humans when we are done with this
        test_list.append( (int(episode), runTest(agent,p)))

    if episode%1000==0: save_weights(agent.weights,episode)
    s=p.getGameState()
    episode+=1


# all done with continuing, need to save the test and cumm attn lists
fi=open(path+'test-res','w')
pickle.dump(test_list,fi)
fi.close()
