import os
from time import sleep

def getCurJobs():
    x=os.system("qstat | grep m180732 | wc -l > .statFile")
    sleep(1)
    fi=open('.statFile')
    l=int(fi.read())
    fi.close()
    return l

def submitJob(alpha, tests):
    # first join the tests into a string
    s=''
    for item in tests:
        s=s+str(item)+'-'
    s=s[:-1]
    
    # now submit the job
    os.environ["A1"]=alpha
    os.environ["J1"]=s
    os.system("qsub -v ALPHA1=$A1,JOBS1=$J1 /mnt/lustre/scratch/m180732/human-continue-training/job-25mar-3.sh")
    print 'submitted',alpha,s

tests={'211434': [29], '214422': [27], '210924': [5, 13], '217224': [29], '186624': [28], '213168': [21, 25], '201776': [27], '186660': [16, 23, 24], '211002': [26], '192712': [13], '213624': [21]}

# going to loop through every alpha and submit a job, never having more than 25 jobs in the queue
# testing ONE job submission right now

jobList={}
for alpha in tests:
    if len(tests[alpha]) > 15:
        split=len(tests[alpha])/2+1
        jobList[alpha]=[tests[alpha][:split], tests[alpha][split:]]
    else:
        jobList[alpha]=[tests[alpha][:]]

alphas=jobList.keys()

#submitJob(alphas[0],jobList[alphas[0]][0])
#exit()


for i in range(len(alphas)):
    # loop through all of the alphas
    curJobs=getCurJobs()
    limit=None
    if len(jobList[alphas[i]]) == 1: limit=25
    else: limit=24
    while curJobs >= limit:
        # need to wait, we will wait 10 minutes (600 on sleep) then check curJobs again
        sleep(600)
        curJobs=getCurJobs()
    
    # no more waiting, so go ahead and submit this alpha's jobs
    submitJob(alphas[i], jobList[alphas[i]][0])
    if len(jobList[alphas[i]]) == 2: submitJob(alphas[i],jobList[alphas[i]][1])  

# all alphas should be done at this point
print 'all done, probably have some failures'

#while l < 25:
# launch job

'''
varTests={}
for key in tests:
    string=''
    for t in tests[key]:
        string=string+str(t)+'-'
    string=string[:-1]
    varTests[key] = string


alphas=tests.keys()
for i in range(0,len(alphas)-1,2):
    # get the first person's alpha jobs
    os.environ["A1"]=alphas[i]
    os.environ["J1"]=varTests[alphas[i]]
    os.environ["A2"]=alphas[i+1]
    os.environ["J2"]=varTests[alphas[i+1]]
    # qsub this
    os.system("qsub -v ALPHA1=$A1,JOBS1=$J1,ALPHA2=$A2,JOBS2=$J2 /mnt/lustre/scratch/m180732/human-continue-training/job-25mar.sh")

print 'submit',alphas[42]
'''
'''#var="|".join(tests.keys())
#os.environ["ALPHAS"]=var
alphaString='('

string=''

# now make the jobs array too
for key in tests:
    alphaString = alphaString + str(key) + '|'
    for t in tests[key]:
        string = string + str(t) + '-'
    # modify the last dash to now be a vertical bar
    string = string[:-1]+'|'

alphaString=alphaString[:-1]+')'
os.environ["ALPHAS"]=alphaString
os.environ["JOBS"]=string


os.system("./25mar-launch.sh")
'''
#tests={'213336': [13, 28], '184392': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '215130': [2, 4, 6, 17, 19, 21], '201674': [2, 5, 13, 17, 20, 28], '211488': [2, 3, 6, 7, 11, 17, 18, 21, 22, 26], '202562': [3, 6, 9, 18, 21, 24], '203138': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '186654': [9, 14, 15, 24, 29, 30], '187080': [2, 4, 5, 12, 13, 17, 19, 20, 27, 28], '194548': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '211434': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '214422': [10, 12, 25, 27], '184092': [3, 4, 13, 14, 18, 19, 28, 29], '185742': [4, 16, 19], '197188': [8, 13, 15, 23, 27, 28, 30], '211704': [2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '205238': [14, 29], '206408': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '215910': [2, 3, 15, 17, 18, 30], '213126': [2, 3, 4, 11, 14, 17, 18, 19, 26, 29], '210924': [2, 3, 4, 5, 13, 17, 18, 19, 20, 28], '215586': [2, 4, 17, 19], '215892': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '194314': [7, 22], '217224': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '186624': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '213168': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '201776': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '186660': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '215226': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '192772': [5, 7, 8, 9, 11, 14, 15, 20, 22, 23, 24, 26, 29, 30], '201656': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '214884': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '210678': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '211002': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '192712': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '211176': [5, 20, 29], '213624': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '212706': [2, 11, 12, 17, 26, 27], '212820': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '193978': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], '210420': [2, 5, 11, 13, 17, 20, 26, 28]}
