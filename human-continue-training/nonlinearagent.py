import pygame
from features import NonLinearFeatures
import cPickle as pickle
from pygame.constants import K_F15,K_a,K_d,K_s,K_w
import random
from constants import *
import numpy as np

def switchAction(act):
    if act == K_w: return 'up'
    elif act == K_a: return 'left'
    elif act == K_s: return 'down'
    elif act == K_d: return 'right'
    else: return 'NOOP'

class NonLinearAgent:
    def __init__(self, actions, features, testing, alpha=0.006, epsilon=0.04, gamma=0.95, weights=None):
        self.actions=actions
        self.features=NonLinearFeatures()
        self.alpha=alpha
        self.epsilon=epsilon
        self.discount=gamma
        self.printOut=False
        self.testing=testing
        self.weights=None
        if not self.testing:
            if weights == None:
                self.weights=[self.features.initializeWeights() for x in range(len(self.actions))]
            else:
                print 'loading up saved weights'
                self.weights=weights
        else:
            self.weights=self.features.loadWeights()

    def featsMatVec(self, feats):
        # takes the features dictionary and creates np matrices/vectors for the math
        featsMat=np.zeros((16,16))
        featsVec=np.zeros((16,1))
        for key in feats.keys():
            if key >= 0 and key < 16:
                # add to our matrix
                featsMat[int(key)][16-(16-int(key)):] = feats[key]
            if key >= 1 and key < 17:
                featsVec[int(key)-1][0] = feats[key]

        # squares of each weight
        sqrVec=np.zeros((1,17))
        for key in feats.keys():
            if key >= 0 and key < 17:
                sqrVec[0][int(key)]=feats[key]*feats[key]

        return featsMat,featsVec,sqrVec

    def linFeats(self, feats):
        linFeats=np.zeros((17,1))
        for key in feats.keys():
            linFeats[key][0] = feats[key]
        return linFeats

    def switchAction(self, action):
        # returns the row number of the weights matrix for this particular action
        if action == K_w:
            return 0
        elif action == K_d:
            return 1
        elif action == K_s:
            return 2
        elif action == K_a:
            return 3
        else:
            # action is NOOP
            return 4

    def getQValue(self, state, action):
        # calculates the Q-value of a the state,action pair
        qval=0.0
        feats=self.features.getFeatures(state)
        # get the matrix representations of the features
        featsMat,featsVec,sqrVec = self.featsMatVec(feats)
        featsLin=self.linFeats(feats)
        
        # get the weights for this action (bilinear and sqr)
        weightsBin=self.weights[self.switchAction(action)][0]
        weightsSqr=self.weights[self.switchAction(action)][1]
        weightsLin=self.weights[self.switchAction(action)][2]

        # inner product of the bilinear
        res=np.dot((weightsBin*featsMat),featsVec)
        # sum of all the values adds to the qval
        qval += sum(res)[0]

        # inner product of the squared weights and features
        res=np.dot(sqrVec,weightsSqr)
        # sum of all of these values add to the qval
        qval += sum(res)[0]

        # inner product of the linear weights and features
        qval+=np.dot(weightsLin,featsLin)[0][0]

        return qval

    def getLegalActions(self, state):
        # this actually will just always return all actions, the environment will disallow any
        # actual moves, however, the human did have this in their arsenal, so need to change
        # returns a list of all possible actions for the agent, mostly just gets rid
        # of any actions that would force the agent off of the screen
        possAct = [x for x in self.actions]
        for act in possAct:
            if act != K_F15:
                # leaving NOOP always
                vector=(0.0,0.0)
                if act == K_a: vector = (-1.0,0.0)
                elif act == K_d: vector = (1.0,0.0)
                elif act == K_w: vector = (0.0,-1.0)
                else: vector = (0.0,1.0)
                (x,y)=(state['frog_x'],state['frog_y'])
                x+=kPlayCellSize[0]*vector[0]
                y+=kPlayCellSize[1]*vector[1]
                if act == K_a and ((x+state['rect_w']) < 0.0): possAct.remove(K_a)
                if act == K_d and ((x+state['rect_w']) > kPlayWidth): possAct.remove(K_d)
                if act == K_w and ((y+state['rect_h']) < 0.0): possAct.remove(K_w)
                if act == K_s and ((y+state['rect_h']) > kPlayHeight-kPlayCellSize[1]): possAct.remove(K_s)

        return possAct

    def getValue(self, state):
        # returns the value of a state, if none, it's a terminal state with value of 0
        if state == None:
            print 'terminal state!'
            return 0.0 # terminal state
        # list of all qvals for every legal action in the state
        qvals=[ self.getQValue(state,action) for action in self.getLegalActions(state)]
        # return the max of the qvals
        return max(qvals)

    def getTrainingPolicy(self, state, printqs):
        # return the argmax_a of Q(s,a), unless they are equal, then return a random action
        qvals=[(self.getQValue(state,action),action) for action in self.getLegalActions(state)]
        if len(set(qvals)) <= 1:
            # all equal, return random action
            print 'random action'
            return random.choice(self.getLegalActions(state))
        else:
            # if we're printing the qvals, then we print out the max
            if printqs: print max(qvals)[0]
            return max(qvals)[1]

    def getPolicy(self, state):
        # only returns the argmax_a Q(s,a)
        qvals=[(self.getQValue(state,action),action) for action in self.getLegalActions(state)]
        return max(qvals)[1]

    def pickAction(self, state, printqs):
        # if we are testing, just return the policy
        # if not, either take an epsilon greedy action or the training policy (argmax_a Q(s,a))
        if not self.testing:
            if random.random() < self.epsilon:
                if printqs: print 'random'
                return random.choice(self.getLegalActions(state))
            else:
                return self.getTrainingPolicy(state, printqs)
        else:
            return self.getPolicy(state)

    def update(self, state, action, nextState, reward):
        # only update if we are not testing
        if not self.testing:
            # get the matrix representation of the features and the weights for this action
            feats=self.features.getFeatures(state)

            featsMat,featsVec,sqrVec = self.featsMatVec(feats)

            weightsBin=self.weights[self.switchAction(action)][0]
            weightsSqr=self.weights[self.switchAction(action)][1]
            weightsLin=self.weights[self.switchAction(action)][2]

            # calculate V(s), Q(s,a) and the correction r+\gamma*V(s) - Q(s,a)
            value=self.getValue(nextState)
            cur=self.getQValue(state,action)
            correction=(reward+(self.discount*value))-cur

            if self.printOut:
                print 'Before Update: Feats:',feats,'Weights:',weightsBin,weightsSqr,weightsLin,'a:',self.alpha,'g:',self.discount,'r:',reward,'Next State:',value,'Q:',cur

            # update the bilinear features deriv according to update rule:
            # w = w + \alpha * correction * deriv (w/ respect to the weight)
            for i in range(16):
                for j in range(16):
                    if j >= i:
                        deriv=featsMat[i][i]*featsVec[j][0]
                        weightsBin[i][j] += float(self.alpha*correction*deriv)

            # update the squared weights
            for i in range(17):
                weightsSqr[i][0] += float(self.alpha * correction * (feats[i]*feats[i]))

            # update the linear weights
            for i in range(17):
                weightsLin[0][i] += float(self.alpha * correction * feats[i])

            # replace the updated weights
            self.weights[self.switchAction(action)] = (weightsBin,weightsSqr,weightsLin)

            if self.printOut:
                print 'After Update: Feats:',feats,'Weights:',self.weights[self.switchAction(action)][0],self.weights[self.switchAction(action)][1],self.weights[self.switchAction(action)][2]
