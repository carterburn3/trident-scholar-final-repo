#!/bin/bash

killall Xvfb

DISPLAYS=()
XPIDS=()
# start up 15 sessions
for i in {101..115};
do
    /usr/bin/Xvfb :$i -nolisten inet6 &
    XPIDS+=($!)
    DISPLAYS+=(:$i)
done

# make alpha job list into an array
IFS='-'; set -f
jobList=($2)
echo ${jobList[@]}
jobLen=${#jobList[@]}

PIDS=()
for (( i=0; i<jobLen; i++ ))
do
    echo "run:" $1 ${jobList[$i]} "DIS" ${DISPLAYS[$i]}
    export DISPLAY=${DISPLAYS[$i]}
    sleep 2
    python /mnt/lustre/scratch/m180732/human-continue-training/quadratic.py -a 0.006 -e 5000 -E 0.04 -g 0.95 -A $1 -N ${jobList[$i]} &
    last=$!
    sleep 2
    p=$(ps axu | grep $last | wc -l)
    while [ $p -eq 1 ]
    do
	echo "Died on" ${jobList[$i]}
	kill -9 ${XPIDS[$i]}
	sleep 1
	/usr/bin/Xvfb ${DISPLAYS[$i]} -nolisten inet6 &
	export DISPLAY=${DISPLAYS[$i]}
	sleep 2
	python /mnt/lustre/scratch/m180732/human-continue-training/quadratic.py -a 0.006 -e 5000 -E 0.04 -g 0.95 -A $1 -N ${jobList[$i]} &
	last=$!
	sleep 2
	p=$(ps axu | grep $last | wc -l)
    done
    PIDS+=($last)
done

sleep 10
echo "PIDS" "${PIDS[@]/#/}"
ps axu | grep python
#wait "${PIDS[@]/#/}"
wait ${PIDS[@]}


killall Xvfb