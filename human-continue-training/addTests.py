from ple import PLE
import frogger
from constants import *
import numpy as np
import pygame
from optparse import OptionParser
from features import NonLinearFeatures
from nonlinearagent import NonLinearAgent
import os
import cPickle as pickle

def parseOptions():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 1.0")
    parser.add_option("-e", "--testEpi",
                      action="store",
                      type="int",
                      dest="testEpi",
                      default=int(0),
                      help="set which test episode")
    parser.add_option("-A", "--alphaNum",
                      action="store",
                      type="str",
                      dest='alphaNum',
                      default='000000',
                      help='define alpha code for testing')
    (options,args) = parser.parse_args()
    return options

options=parseOptions()
if options.alphaNum == '000000':
    print 'PLEASE ENTER ALPHA TO CONTINUE'
    exit(1)
if not options.testEpi:
    print 'PLEASE ENTER A TEST NUMBER'
    exit(1)

humanPath='/mnt/lustre/scratch/m180732/experiment-results/time-warp-res/'+str(options.alphaNum)+'/'
#humanPath='/home/burn/human-systems/experiment-results/time-warp-res/'+str(options.alphaNum)+'/'
if not os.path.exists(humanPath):
    print 'Results for this alpha do not exist! Please check alpha!'
    exit(1)

if not os.path.exists(humanPath+'mid-weight-save/epi-'+str(options.testEpi)):
    print 'No mid-weight-save for this alpha and test number'
    exit(1)

fi=open(humanPath+'mid-weight-save/epi-'+str(options.testEpi))
wts=pickle.load(fi)
fi.close()
# weights loaded up at this point

# run the test with these weights
game=frogger.Frogger()
fps=30
p=PLE(game,fps=fps,force_fps=False,add_noop_action=True,display_screen=True)
agent=NonLinearAgent(p.getActionSet(),None,False,weights=wts)

total=0.0
for i in range(30):
    p.reset_game()
    s=p.getGameState()
    print 'TEST',i
    badStates=0
    while True:
        if p.game_over() or badStates==100:
            break
        if s['frog_y'] > 430.0:
            badStates+=1
        else: badStates=0
        action=agent.getPolicy(s)
        total+=p.act(action)
        s=p.getGameState()

p.reset_game()
toAppend= float(total/30.0)

# load up the human's testRes file
path='/mnt/lustre/scratch/m180732/human-continue-training/testRes/time-warp/'+str(options.alphaNum)+'/'
#path='/home/burn/human-systems/human-continue-training/testRes/time-warp/'+str(options.alphaNum)+'/'
if not os.path.exists(path+'test-res'):
    print 'ERROR. COULD NOT FIND USER PATH'
    exit(1)

fi=open(path+'test-res')
test_list=pickle.load(fi)
fi.close()

# now we insert this result
test_list.append( (int(options.testEpi), toAppend) )
# now sort for ease of use
test_list.sort(key=lambda item: item[0])
# re-save
fi=open(path+'test-res','w')
pickle.dump(test_list, fi)
fi.close()
