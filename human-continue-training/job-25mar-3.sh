#!/bin/bash
# a PBS script to submit one job using the launchOnNode.sh script
# in order to launch multiple test runs of the same experiment, this script
# should be called by another script in a loop fashion
# $1 = display $2 = test number $3 = exp dir
#PBS -l walltime=20:00:00
#PBS -l select=1:mpiprocs=24:ncpus=24

source /home/users/m180732/mypy/bin/activate
aprun -n 1 -d 24 /mnt/lustre/scratch/m180732/human-continue-training/nodeLaunch-3.sh $ALPHA1 $JOBS1